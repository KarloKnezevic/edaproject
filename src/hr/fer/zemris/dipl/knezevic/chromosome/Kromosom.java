package hr.fer.zemris.dipl.knezevic.chromosome;

import java.util.Random;

public class Kromosom implements Comparable<Kromosom> {
	
	byte[] bitovi;
	double fitnes;
	double[] varijable;
	KromosomDekoder dekoder;
	
	public Kromosom(KromosomDekoder dekoder) {
		this.dekoder = dekoder;
		this.bitovi = new byte[dekoder.ukupnoBitova];
		this.fitnes = 0;
		this.varijable = new double[dekoder.brojVarijabli];
	}
	
	public Kromosom(KromosomDekoder dekoder, Random rand) {
		this.dekoder = dekoder;
		this.bitovi = new byte[dekoder.ukupnoBitova];
		this.fitnes = 0;
		this.varijable = new double[dekoder.brojVarijabli];
		for (int i = 0; i < dekoder.ukupnoBitova; i++) {
			this.bitovi[i] = rand.nextBoolean() ? (byte)1 : (byte)0;
		}
	}

	@Override
	public int compareTo(Kromosom o) {
		if (this.fitnes < o.fitnes) {
			return -1;
		}
		
		if (this.fitnes > o.fitnes) {
			return 1;
		}
		
		return 0;
	}

	public byte[] getBitovi() {
		return bitovi;
	}

	public double getFitnes() {
		return fitnes;
	}

	public double[] getVarijable() {
		return varijable;
	}

	public KromosomDekoder getDekoder() {
		return dekoder;
	}

	public void setBitovi(int i, byte bit) {
		if (i >= 0 && i < bitovi.length)
			this.bitovi[i] = bit;
	}
	
	public void setBitovi(byte[] bitovi) {
		this.bitovi = bitovi;
	}

	public void setFitnes(double fitnes) {
		this.fitnes = fitnes;
	}
}