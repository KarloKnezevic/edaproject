package hr.fer.zemris.dipl.knezevic.chromosome;

public class KromosomDekoder {
	
	double[] xMin;
	double[] xMax;
	int[] bitova;
	int[] najveciBinarniBroj; 
	int ukupnoBitova;
	int brojVarijabli;
	
	public KromosomDekoder(int brojVarijabli, int brojBitovaPoVarijabli, double xMin, double xMax) {
		this.brojVarijabli = brojVarijabli;
		this.xMin = new double[brojVarijabli];
		this.xMax = new double[brojVarijabli];
		this.bitova = new int[brojVarijabli];
		this.najveciBinarniBroj = new int[brojVarijabli];
		for (int i = 0; i < brojVarijabli; i++) {
			this.xMin[i] = xMin;
			this.xMax[i] = xMax;
			this.bitova[i] = brojBitovaPoVarijabli;
			this.najveciBinarniBroj[i] = (1 << brojBitovaPoVarijabli) - 1;
		}
		this.ukupnoBitova = brojBitovaPoVarijabli * brojVarijabli;
	}
	
	public void dekodirajKromosom(Kromosom k) {
		int indexBita = 0;
		for (int brojVarijable = 0; brojVarijable < brojVarijabli; brojVarijable++) {
			int prviBit = indexBita;
			int zadnjiBit = prviBit + bitova[brojVarijable] - 1;
			indexBita += bitova[brojVarijable];
			int binarniBroj = 0;
			for (int i = prviBit; i <= zadnjiBit; i++) {
				binarniBroj = binarniBroj*2;
				if (k.bitovi[i] == 1) {
					binarniBroj = binarniBroj+1;
				}
			}
			double vrijednostVarijable = (double) binarniBroj / (double)najveciBinarniBroj[brojVarijable] * 
					(xMax[brojVarijable] - xMin[brojVarijable]) + 
					xMin[brojVarijable];
			k.varijable[brojVarijable] = vrijednostVarijable;
		}
	}
	
	public int getUkupnoBitova() {
		return ukupnoBitova;
	}
}