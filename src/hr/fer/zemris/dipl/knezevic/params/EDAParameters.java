package hr.fer.zemris.dipl.knezevic.params;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import hr.fer.zemris.dipl.knezevic.algorithms.AlgorithmFactory;
import hr.fer.zemris.dipl.knezevic.algorithms.Genetic;
import hr.fer.zemris.dipl.knezevic.algorithms.IAlgorithm;
import hr.fer.zemris.dipl.knezevic.function.FunctionFactory;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.selection.ISelection;
import hr.fer.zemris.dipl.knezevic.selection.SelectionFactory;

public class EDAParameters {

	//broj jedinki u populaciji, >0
	private int velPop;
	//vjerojatnost križanja, [0,1]
	private double vjerKriz;
	//vjerojatnost mutacije, [0,1]
	private double vjerMut;
	//broj varijabli u kromosomu
	private int brojVarijabli; 
	//broj bitova po varijabli, >0, broj bitova karakterizira preciznost rezultata
	private int brojBitovaPoVarijabli;
	//donja granica domene pretraživanja
	private double xMin; 
	//gornja granica domene istraživanja
	private double xMax; 
	//fitness funkcija
	private IFunkcija fitnessFunkcija; 
	//maksimalan broj iteracija algoritma
	private int brojIteracija;
	//stopa učenja u algoritmu CGA, jednaka veličini populacije u GA
	private int velPopGA; 
	//veličina populacije u algoritmu CGA
	private int velPopCGA;
	//granica prihvatljivosti rezultata u CGA, ako je <ε ili >(1-ε), prekida se izvođenje algoritma
	private double ε;
	//postotak jedinki u odnosu na GA na temelju kojih se izvršava algoritam PBIL i UMDA
	private double udioJedinkiZaEstimaciju; 
	//parametar Laplaceovog zaglađivanja
	private double λ; 
	//parametar učenja u PBIL algoritmu
	private double α; 
	//vjerojatnost mutacije vjerojatnosnog vektora u PBIL
	private double mutationProbability; 
	//mutacija vjerojatnosnog vektora u PBIL
	private double mutationShift; 
	//funkcija selekcije
	private ISelection selection;

	//parametri za algoritme
	int[] aktivniAlgoritmi = new int[AlgorithmFactory.getAlgorithms().length];
	//parametri za hibridne algoritme
	int[] aktivniHibridniAlgoritmi = new int[AlgorithmFactory.getHybridAlgorithms().length];

	//parametri za testiranje
	int howManyTestIters;

	/**
	 * Pretpostavljene vrijednosti svih parametara.
	 */
	public EDAParameters() {
		//OPĆI PARAMETRI
		velPop = 50;
		brojVarijabli = 1;
		brojBitovaPoVarijabli = 10;
		xMin = -10;
		xMax = -xMin;
		brojIteracija = 500;
		Arrays.fill(aktivniAlgoritmi, 1);	//svi algoritmi apriori aktivni
		Arrays.fill(aktivniHibridniAlgoritmi, 0);
		if (aktivniHibridniAlgoritmi.length > 1) {
			aktivniHibridniAlgoritmi[0] = 1;
			aktivniHibridniAlgoritmi[1] = 1;
		}

		//FITNES I SELEKCIJA
		fitnessFunkcija = FunctionFactory.getFunction(FunctionFactory.getFunctionNames()[0]);
		selection = SelectionFactory.getSelection(SelectionFactory.getSelectionNames()[0]);

		//GA
		vjerKriz = 0.7;
		vjerMut = 0.005;

		//CGA
		//faktor korekcije je za deset puta manji od broja jedinki u GA
		velPopGA = 10*velPop;
		velPopCGA = 10;
		ε = 0.0005;

		//UMDA i PBIL
		udioJedinkiZaEstimaciju = 0.4;
		λ = 1; //Laplaceovo zaglađivanje

		//PBIL
		α = 0.1;
		mutationProbability = 0.02;
		mutationShift = 0.05;

		//TEST
		howManyTestIters = 5;
	}

	//copy constructor
	public EDAParameters(EDAParameters p) {
		//OPĆI PARAMETRI
		velPop = p.velPop;
		brojVarijabli = p.brojVarijabli;
		brojBitovaPoVarijabli = p.brojBitovaPoVarijabli;
		xMin = p.xMin;
		xMax = p.xMax;
		brojIteracija = p.brojIteracija;
		aktivniAlgoritmi = p.aktivniAlgoritmi;
		aktivniHibridniAlgoritmi = p.aktivniHibridniAlgoritmi;

		//FITNES I SELEKCIJA
		fitnessFunkcija = p.fitnessFunkcija;
		selection = p.selection;

		//GA
		vjerKriz = p.vjerKriz;
		vjerMut = p.vjerMut;

		//CGA
		//faktor korekcije je za deset puta manji od broja jedinki u GA
		velPopGA = p.velPopGA;
		velPopCGA = p.velPopCGA;
		ε = p.ε;

		//UMDA i PBIL
		udioJedinkiZaEstimaciju = p.udioJedinkiZaEstimaciju;
		λ = p.λ; //Laplaceovo zaglađivanje

		//PBIL
		α = p.α;
		mutationProbability = p.mutationProbability;
		mutationShift = p.mutationShift;

		//TEST
		howManyTestIters = p.howManyTestIters;
	}

	public int getVelPopCGA() {
		return velPopCGA;
	}

	public void setVelPopCGA(int velPopCGA) {
		this.velPopCGA = velPopCGA;
	}

	public int getVelPop() {
		return velPop;
	}

	public void setVelPop(int velPop) {
		this.velPop = velPop;
	}

	public double getVjerKriz() {
		return vjerKriz;
	}

	public void setVjerKriz(double vjerKriz) {
		this.vjerKriz = vjerKriz;
	}

	public double getVjerMut() {
		return vjerMut;
	}

	public void setVjerMut(double vjerMut) {
		this.vjerMut = vjerMut;
	}

	public int getBrojVarijabli() {
		return brojVarijabli;
	}

	public void setBrojVarijabli(int brojVarijabli) {
		this.brojVarijabli = brojVarijabli;
	}

	public int getBrojBitovaPoVarijabli() {
		return brojBitovaPoVarijabli;
	}

	public void setBrojBitovaPoVarijabli(int brojBitovaPoVarijabli) {
		this.brojBitovaPoVarijabli = brojBitovaPoVarijabli;
	}

	public double getxMin() {
		return xMin;
	}

	public void setxMin(double xMin) {
		this.xMin = xMin;
	}

	public double getxMax() {
		return xMax;
	}

	public void setxMax(double xMax) {
		this.xMax = xMax;
	}

	public IFunkcija getFitnessFunkcija() {
		return fitnessFunkcija;
	}

	public void setFitnessFunkcija(IFunkcija fitnessFunkcija) {
		this.fitnessFunkcija = fitnessFunkcija;
	}

	public int getBrojIteracija() {
		return brojIteracija;
	}

	public void setBrojIteracija(int brojIteracija) {
		this.brojIteracija = brojIteracija;
	}

	public int getVelPopGA() {
		return velPopGA;
	}

	public void setVelPopGA(int velPopGA) {
		this.velPopGA = velPopGA;
	}

	public double getΕ() {
		return ε;
	}

	public void setΕ(double ε) {
		this.ε = ε;
	}

	public double getUdioJedinkiZaEstimaciju() {
		return udioJedinkiZaEstimaciju;
	}

	public void setUdioJedinkiZaEstimaciju(double udioJedinkiZaEstimaciju) {
		this.udioJedinkiZaEstimaciju = udioJedinkiZaEstimaciju;
	}

	public double getΛ() {
		return λ;
	}

	public void setΛ(double λ) {
		this.λ = λ;
	}

	public double getΑ() {
		return α;
	}

	public void setΑ(double α) {
		this.α = α;
	}

	public double getMutationProbability() {
		return mutationProbability;
	}

	public void setMutationProbability(double mutationProbability) {
		this.mutationProbability = mutationProbability;
	}

	public double getMutationShift() {
		return mutationShift;
	}

	public void setMutationShift(double mutationShift) {
		this.mutationShift = mutationShift;
	}

	public ISelection getSelection() {
		return selection;
	}

	public void setSelection(ISelection selection) {
		this.selection = selection;
	}

	public int getAlgorithmCount() {
		return aktivniAlgoritmi.length;
	}

	public IAlgorithm[] getAlgoritmi() {
		List<IAlgorithm> listaAktivnihAlgoritama = new ArrayList<IAlgorithm>();
		IAlgorithm[] algoritmi = AlgorithmFactory.getAlgorithms();
		for (int i = 0; i < aktivniAlgoritmi.length; i++) {
			if (aktivniAlgoritmi[i] == 1) {
				listaAktivnihAlgoritama.add(algoritmi[i]);
			}
		}

		IAlgorithm[] nizAktivnihAlgoritmi = listaAktivnihAlgoritama.toArray(new IAlgorithm[listaAktivnihAlgoritama.size()]);
		return nizAktivnihAlgoritmi;
	}

	public void setActiveAlgorithm(int algorithm, boolean activate) {
		aktivniAlgoritmi[algorithm] = activate ? 1 : 0;
	}

	public String[] getAlgorithmsName() {
		return AlgorithmFactory.getAlgorithmNames();
	}

	public int getHybridAlgorithmCount() {
		return aktivniHibridniAlgoritmi.length>1 ? aktivniHibridniAlgoritmi.length : 0;
	}

	public String[] getHybridAlgorithmNames() {
		return AlgorithmFactory.getHybridAlgorithmNames();
	}

	public int[] getHybridSelected() {
		return aktivniHibridniAlgoritmi;
	}

	public Genetic[] getActiveHybridAlgorithm() {
		Genetic[] array = new Genetic[2];
		int cnt = 0;
		for (int i = 0; i < aktivniHibridniAlgoritmi.length; i++) {
			if (aktivniHibridniAlgoritmi[i] == 1) {
				array[cnt] = AlgorithmFactory.getHybridAlgorithms()[i];
				cnt++;
			}
			if (cnt > 1) break;
		}
		return array;
	}

	public int setActiveHybridAlgorithm(int algorithm, boolean activate) {
		aktivniHibridniAlgoritmi[algorithm] = activate ? 1 : 0;

		int sum = 0;
		for (int i = 0; i < aktivniHibridniAlgoritmi.length; i++) sum += aktivniHibridniAlgoritmi[i];
		return sum;
	}

	public void setHowManyTestIters(int howManyTestIters) {
		this.howManyTestIters = howManyTestIters;
	}

	public int getHowManyTestIters() {
		return howManyTestIters;
	}
}