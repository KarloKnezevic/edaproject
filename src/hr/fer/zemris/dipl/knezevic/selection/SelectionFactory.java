package hr.fer.zemris.dipl.knezevic.selection;

public class SelectionFactory {
	//dodati u selection novu selekciju
	private static ISelection[] selection = { new FirstNBest(), new KTournament(3), new Proportional() };
	private static String[] names = new String [selection.length];
	
	public static ISelection getSelection(String selectionName) {
		for (int i = 0; i < selection.length; i++) {
			if (selection[i].getName().equals(selectionName))
				return selection[i];
		}
		return null;
	}
	
	public static String[] getSelectionNames() {
		for (int i = 0; i < selection.length; i++) {
			names[i] = selection[i].getName();
		}
		return names;
	}
}