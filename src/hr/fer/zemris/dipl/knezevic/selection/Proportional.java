package hr.fer.zemris.dipl.knezevic.selection;

import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;

public class Proportional implements ISelection {

	private String name = "Proportional Selection";

	@Override
	public void selektirajJedinke(Kromosom[] populacija, Kromosom[] selektiraneJedinke) {
		Random rand = new Random();
		double sumaDobrota = 0;
		double najvecaVrijednost = 0;

		for (int i = 0; i < populacija.length; i++) {
			sumaDobrota += populacija[i].getFitnes();
			if (i==0 || najvecaVrijednost < populacija[i].getFitnes()) {
				najvecaVrijednost = populacija[i].getFitnes();
			}
		}

		sumaDobrota = populacija.length * najvecaVrijednost - sumaDobrota;

		for (int j = 0; j < selektiraneJedinke.length; j++) {
			double slucajniBroj = rand.nextDouble()*sumaDobrota;
			double akumuliranaSuma = 0;
			boolean assigned = false;
			for (int i = 0; i < populacija.length; i++) {
				akumuliranaSuma += najvecaVrijednost - populacija[i].getFitnes();
				if (slucajniBroj < akumuliranaSuma) { 
					selektiraneJedinke[j] = populacija[i]; 
					assigned = true; 
					break;
				}
			}
			if (!assigned)
				selektiraneJedinke[j] = populacija[populacija.length-1];
		}
	}

	@Override
	public String getName() {
		return name;
	}

}
