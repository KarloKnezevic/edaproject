package hr.fer.zemris.dipl.knezevic.selection;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;

public class FirstNBest implements ISelection {
	
	private String name = "First N Best Selection";

	@Override
	public void selektirajJedinke(Kromosom[] populacija, Kromosom[] selektiraneJedinke) {
		
		for (int i = 0; i < selektiraneJedinke.length; i++) {
			selektiraneJedinke[i].setBitovi(populacija[i].getBitovi());
		}
	}

	@Override
	public String getName() {
		return name;
	}

}
