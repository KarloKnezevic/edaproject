package hr.fer.zemris.dipl.knezevic.selection;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;

public interface ISelection {
	
	public void selektirajJedinke(Kromosom[] populacija, Kromosom[] selektiraneJedinke);
	
	public String getName();

}
