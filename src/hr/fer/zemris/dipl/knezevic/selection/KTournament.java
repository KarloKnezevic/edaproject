package hr.fer.zemris.dipl.knezevic.selection;

import java.util.Arrays;
import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;

public class KTournament implements ISelection {
	
	private int K;
	private String name = "Tournament Selection";
	
	public KTournament(int K) {
		
		this.K = K%2==1 ? K : (K+1);
	}

	@Override
	public void selektirajJedinke(Kromosom[] populacija, Kromosom[] selektiraneJedinke) {
		
		Random rand = new Random();
		Kromosom[] turnirskeJedinke = new Kromosom[K];
		int jedinka;
		
		for (int i = 0; i < selektiraneJedinke.length; i++) {
			for (int j = 0; j < K; j++) {
				jedinka = rand.nextInt(populacija.length);
				turnirskeJedinke[j] = populacija[jedinka];
			}
			Arrays.sort(turnirskeJedinke);
			selektiraneJedinke[i] = turnirskeJedinke[0];
		}
		
	}

	@Override
	public String getName() {
		return K + " " + name;
	}

}
