package hr.fer.zemris.dipl.knezevic.function;

public class F2 implements IFunkcija {
	
	private String name = "F2";

	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		double vrijednost = 0;
		for (int i = 0; i < n; i++) {
			vrijednost += varijable[i];
		}
		return vrijednost;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		return "f(x)=sum(int(xi)), Granice:[-5.12, 5.12]";
	}

}
