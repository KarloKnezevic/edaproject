package hr.fer.zemris.dipl.knezevic.function;

public class FunctionFactory {
	//dodati u function novu funkciju
	private static IFunkcija[] function = { new Rastrigin(), new Griewank(), new Schwefel(), 
		new Schaffer(), new Rosenbrock(), new F1(), new F2(), new F3(), new F4() };
	private static String[] names = new String[function.length];

	public static IFunkcija getFunction(String functionName) {
		for (int i = 0; i < function.length; i++) {
			if (function[i].getName().equals(functionName))
				return function[i];
		}
		return null;
	}
	
	public static String[] getFunctionNames() {
		for (int i = 0; i < function.length; i++) {
			names[i] = function[i].getName();
		}
		return names;
	}
}