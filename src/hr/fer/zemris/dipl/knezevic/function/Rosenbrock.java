package hr.fer.zemris.dipl.knezevic.function;

public class Rosenbrock implements IFunkcija {
	
	private String name = "Rosenbrock";

	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		double sum = 0;
		for (int i = 0; i < n-1; i++) {
			sum += (100*Math.pow(varijable[i+1]-varijable[i]*varijable[i], 2)) + (1-varijable[i])*(1-varijable[i]);
		}
		return sum;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		String info = "Globalni minimum je za f(0,...,0)=0";
		return info;
	}
}