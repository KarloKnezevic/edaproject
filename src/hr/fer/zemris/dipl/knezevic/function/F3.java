package hr.fer.zemris.dipl.knezevic.function;

public class F3 implements IFunkcija {

	private String name = "F3";
	
	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		double vrijednost = 0;
		for (int i = 0; i < n; i++) {
			vrijednost += (i+1)*varijable[i]*varijable[i];
		}
		return vrijednost;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		return "f(x)=sum(i*xi^2), Granice:[-5.12, 5.12]";
	}

}
