package hr.fer.zemris.dipl.knezevic.function;

public class Schaffer implements IFunkcija {
	
	private String name = "Schaffer";

	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		
		double sum = 0;
		for (int i = 0; i < n; i++) {
			sum += varijable[i]*varijable[i];
		}
		double result = 0.5 + (Math.pow(Math.sin(Math.sqrt(sum)), 2)-0.5)/Math.pow((1 + 0.001*sum),2);
		return result;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		String info = "Globalni minimum je za f(0,...,0)=0";
		return info;
	}

}
