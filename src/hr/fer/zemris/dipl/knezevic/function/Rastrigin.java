package hr.fer.zemris.dipl.knezevic.function;

public class Rastrigin implements IFunkcija {
	
	private String name = "Rastring";

	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		double vrijednost = 10*n;
		for (int i = 0; i < n; i++) {
			vrijednost += varijable[i]*varijable[i] - 10*Math.cos(2*Math.PI * varijable[i]);
		}
		return vrijednost;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		String info = "Globalni minimum: x{i}=0, f(x{i})=0, preporučen prostor pretrage: [-10, 10], više informacija: http://en.wikipedia.org/wiki/Rastrigin_function";
		return info;
	}

}
