package hr.fer.zemris.dipl.knezevic.function;

public interface IFunkcija {
	
	public double izracunaj (double[] varijable);
	
	public String getName();
	
	public String getInfo();

}
