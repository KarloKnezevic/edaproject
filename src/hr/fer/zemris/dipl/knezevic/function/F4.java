package hr.fer.zemris.dipl.knezevic.function;

public class F4 implements IFunkcija {
	
	private String name = "F4";

	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		double vrijednost = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= i; j++) {
				vrijednost += varijable[j]*varijable[j];
			}
		}
		return vrijednost;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		return "f(x)=sum{1,D}(sum{1,i}(xj*xj)), Granice:[-65536, 65536]";
	}

}
