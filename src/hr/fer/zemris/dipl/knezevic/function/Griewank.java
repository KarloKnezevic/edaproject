package hr.fer.zemris.dipl.knezevic.function;

public class Griewank implements IFunkcija {
	
	private String name = "Griewank";

	@Override
	public double izracunaj(double[] varijable) {

		double suma = 0;
		double produkt = 1;
		
		for (int i = 0; i < varijable.length; i++) {
			suma += varijable[i]*varijable[i];
			produkt *= Math.cos(varijable[i] / Math.sqrt(i+1));
		}
		
		return 1 + (1.0/4000.0)*suma - produkt;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		String info = "Globalni minimum: x{i}=0, f(x{i})=0, preporučen prostor pretrage: [-600, 600], više informacija: http://mathworld.wolfram.com/GriewankFunction.html";
		return info;
	}

}
