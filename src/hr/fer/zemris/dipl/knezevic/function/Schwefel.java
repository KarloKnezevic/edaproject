package hr.fer.zemris.dipl.knezevic.function;

public class Schwefel implements IFunkcija {
	
	private String name = "Schwefel";

	@Override
	public double izracunaj(double[] varijable) {
		int n = varijable.length;
		double vrijednost = 0;
		for (int i = 0; i < n; i++) {
			vrijednost += -varijable[i]*Math.sin( Math.sqrt( Math.abs( varijable[i] ) ) );
		}
		return vrijednost/n;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInfo() {
		String info = "Optimim funkcije ovisi o intervalu pretrage. Preporučen prostor pretrage: [-512, 512]. Za [+-512], f(x{i}) = -418.9828, za [+-500], f(x{i}) = 0.";
		return info;
	}
}
