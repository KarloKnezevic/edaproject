package hr.fer.zemris.dipl.knezevic.main;

import javax.swing.SwingUtilities;
import hr.fer.zemris.dipl.knezevic.gui.GUIWindow;

/**
 * Estimation of Distribution Algorithms
 * @author Karlo Knezevic
 *
 */
public class EstimationOfDistributionAlgorithm {

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new GUIWindow().setVisible(true);
			}
		});
	}
}