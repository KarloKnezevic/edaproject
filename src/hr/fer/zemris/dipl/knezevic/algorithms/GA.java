package hr.fer.zemris.dipl.knezevic.algorithms;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.ISelection;

import java.util.Arrays;
import java.util.Random;

/**
 * Genetic Algorithm
 * @author Karlo Knezevic
 *
 */
public class GA extends Genetic {
	
	public GA() {
		algorithmName = "Genetic algorithm";
	}
	
	@Override
	public void start(EDAParameters params) {
		startStopwatch();
		
		int VEL_POP = params.getVelPop();
		double VJER_KRIZ = params.getVjerKriz();
		double VJER_MUT = params.getVjerMut();
		
		Random rand = new Random();
		
		KromosomDekoder dekoder = new KromosomDekoder(params.getBrojVarijabli(), params.getBrojBitovaPoVarijabli(), params.getxMin(), params.getxMax());
		
		Kromosom[] populacija = stvoriPopulaciju(VEL_POP, dekoder, rand);
		Kromosom[] novaGeneracija = stvoriPopulaciju(VEL_POP, dekoder, null);
		
		IFunkcija funkcija = params.getFitnessFunkcija();
		ISelection selekcija = params.getSelection();
		
		evaluirajPopulaciju(populacija, funkcija);
		

		bestCodomainValuesPerIteration = new double[params.getBrojIteracija()];
		double minimum = 0;
		
		Kromosom najbolji = null;
		Kromosom[] roditelji = stvoriPopulaciju(2, dekoder, null);
		for (int generacija = 0; generacija < params.getBrojIteracija(); generacija++) {
			
			Arrays.sort(populacija);

			kopiraj(populacija[0], novaGeneracija[0]);
			kopiraj(populacija[1], novaGeneracija[1]);
			
			for (int i = 1; i < VEL_POP/2; i++) {
				selekcija.selektirajJedinke(populacija, roditelji);
				Kromosom dijete1 = novaGeneracija[2*i];
				Kromosom dijete2 = novaGeneracija[2*i+1];
				
				krizaj1TockaPrijeloma(VJER_KRIZ, roditelji[0], roditelji[1], dijete1, dijete2, rand);
				mutiraj(VJER_MUT, dijete1, rand);
				mutiraj(VJER_MUT, dijete2, rand);
			}
			
			Kromosom[] pomocni = populacija;
			populacija = novaGeneracija;
			novaGeneracija = pomocni;
			
			evaluirajPopulaciju(populacija, funkcija);
			
			for (int i = 0; i < populacija.length; i++) {
				if (i==0 || najbolji.getFitnes() > populacija[i].getFitnes()) {
					najbolji = populacija[i];
				}
			}
			
			bestCodomainValuesPerIteration[generacija] = funkcija.izracunaj(najbolji.getVarijable());
			
			if (generacija == 0) {
				minimum = bestCodomainValuesPerIteration[generacija];
			} else {
				if (bestCodomainValuesPerIteration[generacija] < minimum) {
					bestValueIteration = generacija;
					minimum = bestCodomainValuesPerIteration[generacija];
				}
			}
			
		}
		
		domainSolution = najbolji.getVarijable();
		codomainSolution = funkcija.izracunaj(domainSolution);
		
		stopStopwatch();
		
		/*System.out.println(algorithmName + "|" + selekcija.getName() + "|" + funkcija.getName() + "| f(" + 
				Arrays.toString(domainSolution) + ") = " + funkcija.izracunaj(domainSolution) + "|" + executionTime + " ms | " + bestValueIteration);*/
	}
	
	@Override
	public Kromosom[] jezgra(Kromosom[] populacija, KromosomDekoder dekoder, EDAParameters params, Random rand) {
		
		Arrays.sort(populacija);
		
		Kromosom[] roditelji = stvoriPopulaciju(2, dekoder, null);
		Kromosom[] novaGeneracija = stvoriPopulaciju(params.getVelPop(), dekoder, null);
		
		ISelection selekcija = params.getSelection();

		kopiraj(populacija[0], novaGeneracija[0]);
		kopiraj(populacija[1], novaGeneracija[1]);
		
		for (int i = 1; i < params.getVelPop()/2; i++) {
			selekcija.selektirajJedinke(populacija, roditelji);
			Kromosom dijete1 = novaGeneracija[2*i];
			Kromosom dijete2 = novaGeneracija[2*i+1];
			
			krizaj1TockaPrijeloma(params.getVjerKriz(), roditelji[0], roditelji[1], dijete1, dijete2, rand);
			mutiraj(params.getVjerMut(), dijete1, rand);
			mutiraj(params.getVjerMut(), dijete2, rand);
		}		
		return novaGeneracija;	
	}
	
	private void kopiraj(Kromosom original, Kromosom kopija) {
		for (int i = 0; i < original.getBitovi().length; i++) {
			kopija.setBitovi(i, original.getBitovi()[i]);
		}
	}
	
	private void krizaj1TockaPrijeloma( double vjerKriz, Kromosom roditelj1, Kromosom roditelj2, Kromosom dijete1, Kromosom dijete2, Random rand) {
		if (rand.nextFloat() <= vjerKriz) {
			int tockaPrijeloma = rand.nextInt(roditelj1.getDekoder().getUkupnoBitova()-1) + 1;
			for (int i = 0; i < tockaPrijeloma; i++) {
				dijete1.setBitovi(i, roditelj1.getBitovi()[i]);
				dijete2.setBitovi(i, roditelj2.getBitovi()[i]);
			}
			for (int i = tockaPrijeloma; i < roditelj1.getDekoder().getUkupnoBitova(); i++) {
				dijete1.setBitovi(i, roditelj1.getBitovi()[i]);
				dijete2.setBitovi(i, roditelj2.getBitovi()[i]);
			}
		} else {
			for (int i = 0; i < roditelj1.getDekoder().getUkupnoBitova(); i++) {
				dijete1.setBitovi(i, roditelj1.getBitovi()[i]);
				dijete2.setBitovi(i, roditelj2.getBitovi()[i]);
			}
		}
	}
	
	private void mutiraj(double vjerMut, Kromosom dijete, Random rand) {
		for (int i = 0; i < dijete.getDekoder().getUkupnoBitova(); i++) {
			if (rand.nextFloat() <= vjerMut) {
				dijete.setBitovi(i, (byte)(1-dijete.getBitovi()[i]));
			}
		}
	}

	@Override
	public String getAlgorithmName() {
		return algorithmName;
	}

	@Override
	public double getCodomainSolution() {
		return codomainSolution;
	}

	@Override
	public double[] getDomainSolution() {
		return domainSolution;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public double[] getBestCodomainValuesPerIteration() {
		return bestCodomainValuesPerIteration;
	}

	@Override
	public int getBestVelueIteration() {
		return bestValueIteration;
	}
}