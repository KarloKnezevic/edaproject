package hr.fer.zemris.dipl.knezevic.algorithms;

import java.util.Arrays;
import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.ISelection;

/**
 * Population Based Incremental Learning 
 * The selected individuals are used to only update the previous estimates.
 * p[i](x) = (1 - α)*p[i-1] + α*UMDA.
 * α is learning rate and it's in <0,1].
 * @author Karlo Knezevic
 *
 */
public class PBIL extends Genetic {
	
	public PBIL() {
		algorithmName = "Population-based incremental learning";
	}

	@Override
	public void start(EDAParameters params) {
		startStopwatch();

		int VEL_POP = params.getVelPop();
		int brojJedinki = (int)Math.ceil(params.getUdioJedinkiZaEstimaciju()*VEL_POP);

		Random rand = new Random();

		KromosomDekoder dekoder = new KromosomDekoder(params.getBrojVarijabli(), params.getBrojBitovaPoVarijabli(), params.getxMin(), params.getxMax());

		Kromosom[] populacija = stvoriPopulaciju(VEL_POP, dekoder, rand);
		Kromosom[] selektiraneJedinke = stvoriPopulaciju(brojJedinki, dekoder, null);
		double[] procjenjeneVjerojatnostiGena = new double[params.getBrojVarijabli()*params.getBrojBitovaPoVarijabli()];

		//50% of using 1 or 0 value of gene
		Arrays.fill(procjenjeneVjerojatnostiGena, 0.5);

		IFunkcija funkcija = params.getFitnessFunkcija();
		ISelection selection = params.getSelection();

		Kromosom najbolji = null;
		evaluirajPopulaciju(populacija, funkcija);
		
		bestCodomainValuesPerIteration = new double[params.getBrojIteracija()];
		double minimum = 0;

		for (int generacija = 0; generacija < params.getBrojIteracija(); generacija++) {

			Arrays.sort(populacija);

			selection.selektirajJedinke(populacija, selektiraneJedinke);

			//PBIL learning
			procijeniVjerojatnostiGenaSelektiranihKromosoma(selektiraneJedinke, procjenjeneVjerojatnostiGena, params.getΛ(), params.getΑ(), 
					params.getMutationProbability(), params.getMutationShift(), rand);

			//u novu generaciju prenesi 2 najboljih jedinki; elitizam
			stvoriNovuGeneraciju(populacija, procjenjeneVjerojatnostiGena, rand, 2);

			evaluirajPopulaciju(populacija, funkcija);

			for (int i = 0; i < populacija.length; i++) {
				if (i==0 || najbolji.getFitnes() > populacija[i].getFitnes()) {
					najbolji = populacija[i];
				}
			}
			
			bestCodomainValuesPerIteration[generacija] = funkcija.izracunaj(najbolji.getVarijable());
			
			if (generacija == 0) {
				minimum = bestCodomainValuesPerIteration[generacija];
			} else {
				if (bestCodomainValuesPerIteration[generacija] < minimum) {
					bestValueIteration = generacija;
					minimum = bestCodomainValuesPerIteration[generacija];
				}
			}
		}
		
		domainSolution = najbolji.getVarijable();
		codomainSolution = funkcija.izracunaj(domainSolution);
		
		stopStopwatch();
		
		/*System.out.println(algorithmName + "|" + selection.getName() + "|" + funkcija.getName() + "| f(" + 
				Arrays.toString(domainSolution) + ") = " + funkcija.izracunaj(domainSolution) + "|" + executionTime + " ms | " + bestValueIteration);*/
	}
	
	@Override
	public Kromosom[] jezgra(Kromosom[] populacija, KromosomDekoder dekoder, EDAParameters params, Random rand) {
		
		Arrays.sort(populacija);

		int brojJedinki = (int)Math.ceil(params.getUdioJedinkiZaEstimaciju()*params.getVelPop());
		Kromosom[] selektiraneJedinke = stvoriPopulaciju(brojJedinki, dekoder, null);
		double[] procjenjeneVjerojatnostiGena = new double[params.getBrojVarijabli()*params.getBrojBitovaPoVarijabli()];
		Kromosom[] novaGeneracija = stvoriPopulaciju(params.getVelPop(), dekoder, null);

		ISelection selection = params.getSelection();
		selection.selektirajJedinke(populacija, selektiraneJedinke);

		procijeniVjerojatnostiGenaSelektiranihKromosoma(selektiraneJedinke, procjenjeneVjerojatnostiGena, params.getΛ(), params.getΑ(), 
				params.getMutationProbability(), params.getMutationShift(), rand);
		
		//elitizam
		novaGeneracija[0] = populacija[0];
		novaGeneracija[1] = populacija[1];

		stvoriNovuGeneraciju(novaGeneracija, procjenjeneVjerojatnostiGena, rand, 2);
		
		return novaGeneracija;
	}

	private void stvoriNovuGeneraciju(Kromosom[] populacija, double[] procjenjeneVjerojatnostiGena, Random rand, int brojSacuvanihJedinki) {
		for (int i = brojSacuvanihJedinki; i < populacija.length; i++) {
			for (int j = 0; j < procjenjeneVjerojatnostiGena.length; j++) {
				if (rand.nextDouble() <= procjenjeneVjerojatnostiGena[j]) {
					populacija[i].setBitovi(j, (byte)1);
				} else {
					populacija[i].setBitovi(j, (byte)0);
				}
			}
		}
	}

	/**
	 * Using Laplace smoothing, λ=1
	 * Using learning rate, α in <0,1].
	 * Mutation is used for probability vector. Mutation shift deternimes changing value per gene probability if it's chosen.
	 * @param selektiraneJedinke
	 * @param procjenjeneVjerojatnostiGena
	 */
	private void procijeniVjerojatnostiGenaSelektiranihKromosoma(Kromosom[] selektiraneJedinke, double[] procjenjeneVjerojatnostiGena, 
			double λ, double α, double mutationProbability, double mutationShift, Random rand) {

		double[] procjenjeneVjerojatnostiGenaProsleIteracije = procjenjeneVjerojatnostiGena;
		Arrays.fill(procjenjeneVjerojatnostiGena, 0);
		for (int i = 0; i < selektiraneJedinke.length; i++) {
			for (int j = 0; j < procjenjeneVjerojatnostiGena.length; j++) {
				procjenjeneVjerojatnostiGena[j] += selektiraneJedinke[i].getBitovi()[j];
			}
		}

		//use Laplace smoothnig and learning rate
		double r = 2*λ;
		for (int i = 0; i < procjenjeneVjerojatnostiGena.length; i++) {
			procjenjeneVjerojatnostiGena[i] = (procjenjeneVjerojatnostiGena[i] + λ) / (selektiraneJedinke.length + r);
			//using paste information
			procjenjeneVjerojatnostiGena[i] = (1-α)*procjenjeneVjerojatnostiGenaProsleIteracije[i] + α*procjenjeneVjerojatnostiGena[i];
			//mutation
			if (rand.nextDouble() < mutationProbability) {
				procjenjeneVjerojatnostiGena[i] = procjenjeneVjerojatnostiGena[i]*(1-mutationShift) + (rand.nextBoolean() ? 1d: 0d)*mutationShift;
			}
		}
	}

	@Override
	public double[] getBestCodomainValuesPerIteration() {
		return bestCodomainValuesPerIteration;
	}

	@Override
	public String getAlgorithmName() {
		return algorithmName;
	}

	@Override
	public double getCodomainSolution() {
		return codomainSolution;
	}

	@Override
	public double[] getDomainSolution() {
		return domainSolution;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public int getBestVelueIteration() {
		return bestValueIteration;
	}
}