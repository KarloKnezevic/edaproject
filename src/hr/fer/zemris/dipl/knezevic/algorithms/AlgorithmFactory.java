package hr.fer.zemris.dipl.knezevic.algorithms;

public class AlgorithmFactory {
	//dodati novi algoritam u algorithms
	private static IAlgorithm[] algorithms = { new GA(), new CGA(), new PBIL(), new UMDA(), new MIMIC(), new HybridGAUMDA() };
	//dodati algoritme koji overridaju metodu jezgra
	private static Genetic[] hybridAlgorithms = { new GA(), new UMDA(), new PBIL() };
	
	public static IAlgorithm[] getAlgorithms() {
		return algorithms;
	}
	
	public static Genetic[] getHybridAlgorithms() {
		return hybridAlgorithms;
	}
	
	public static Genetic getHybridAlgorithm(String name) {
		for (int i = 0; i < hybridAlgorithms.length; i++) {
			if (hybridAlgorithms[i].getAlgorithmName().equals(name))
				return hybridAlgorithms[i];
		}
		return null;
	}
	
	public static IAlgorithm getAlgorithm(String name) {
		for (int i = 0; i < algorithms.length; i++) {
			if (algorithms[i].getAlgorithmName().equals(name))
				return algorithms[i];
		}
		return null;
	}
	
	public static String[] getHybridAlgorithmNames() {
		String[] names = new String[hybridAlgorithms.length];
		for (int i = 0; i < hybridAlgorithms.length; i++) {
			names[i] = hybridAlgorithms[i].getAlgorithmName();
		}
		return names;
	}
	
	public static String[] getAlgorithmNames() {
		String[] names = new String[algorithms.length];
		for (int i = 0; i < algorithms.length; i++) {
			names[i] = algorithms[i].getAlgorithmName();
		}
		return names;
	}
}
