package hr.fer.zemris.dipl.knezevic.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;

public class CGA extends Genetic {
	
	public CGA() {
		algorithmName = "Compact Genetic Algorithm";
	}

	@Override
	public void start(EDAParameters params) {
		startStopwatch();

		int VEL_POP = params.getVelPopCGA();

		Random rand = new Random();

		KromosomDekoder dekoder = new KromosomDekoder(params.getBrojVarijabli(), params.getBrojBitovaPoVarijabli(), params.getxMin(), params.getxMax());

		Kromosom[] populacija = stvoriPopulaciju(VEL_POP, dekoder, null);
		double[] procjenjeneVjerojatnostiGena = new double[params.getBrojVarijabli()*params.getBrojBitovaPoVarijabli()];
		Arrays.fill(procjenjeneVjerojatnostiGena, 0.5);

		IFunkcija funkcija = params.getFitnessFunkcija();

		Kromosom rjesenje = null;
		Kromosom best = null;
		boolean nijeZadovoljenaKonvergencija;
		int iteracija = 0;
		
		//dodano radi crtanja
		List<Double> listBestCodomainValuesPerIteration = new ArrayList<>();
		//bestCodomainValuesPerIteration = new double[params.getBrojIteracija()];

		do {

			stvoriNovuGeneraciju(populacija, procjenjeneVjerojatnostiGena, rand);

			evaluirajPopulaciju(populacija, funkcija);

			Arrays.sort(populacija);

			Kromosom pobjednik = populacija[0];
			Kromosom gubitnik;
			for (int i = 1; i < VEL_POP; i++) {
				gubitnik = populacija[i];
				promijeniProcjenjeneVjerojatnostiGena(pobjednik, gubitnik, params.getVelPopGA(), procjenjeneVjerojatnostiGena);
			}

			nijeZadovoljenaKonvergencija = ispitajKonvergenciju(procjenjeneVjerojatnostiGena, params.getΕ());
			iteracija++;
			
			//dodadno radi crtanja
			rjesenje = binarizirajVjerojatnosti(procjenjeneVjerojatnostiGena, dekoder, params.getΕ(), rand);
			evaluirajJedinku(rjesenje, funkcija);
			
			//elitizam
			if (iteracija == 1) {
				best = rjesenje;
			} else if (rjesenje.getFitnes() < best.getFitnes()) {
				best = rjesenje;
			}
			
			listBestCodomainValuesPerIteration.add(funkcija.izracunaj(best.getVarijable()));
			//bestCodomainValuesPerIteration[iteracija-1] = funkcija.izracunaj(rjesenje.getVarijable());
			//kraj dodavanja za crtanje

		} while (nijeZadovoljenaKonvergencija && iteracija < params.getBrojIteracija());
		
		rjesenje = binarizirajVjerojatnosti(procjenjeneVjerojatnostiGena, dekoder, params.getΕ(), rand);
		evaluirajJedinku(rjesenje, funkcija);
		
		if (rjesenje.getFitnes() < best.getFitnes()) {
			best = rjesenje;
		}
		
		domainSolution = best.getVarijable();
		codomainSolution = funkcija.izracunaj(domainSolution);
		bestValueIteration = iteracija;
		
		bestCodomainValuesPerIteration = new double[listBestCodomainValuesPerIteration.size()];
		for (int i = 0; i < bestCodomainValuesPerIteration.length; i++) bestCodomainValuesPerIteration[i] = listBestCodomainValuesPerIteration.get(i).doubleValue();
		
		stopStopwatch();
		
		/*System.out.println(algorithmName + "| no specific selection |" + funkcija.getName() + "| f(" + 
				Arrays.toString(domainSolution) + ") = " + funkcija.izracunaj(domainSolution) + "|" + executionTime + " ms | " + bestValueIteration);*/
	}

	private void stvoriNovuGeneraciju(Kromosom[] populacija, double[] procjenjeneVjerojatnostiGena, Random rand) {
		for (int i = 0; i < populacija.length; i++) {
			for (int j = 0; j < procjenjeneVjerojatnostiGena.length; j++) {
				if (rand.nextDouble() <= procjenjeneVjerojatnostiGena[j]) {
					populacija[i].setBitovi(j, (byte)1);
				} else {
					populacija[i].setBitovi(j, (byte)0);
				}
			}
		}
	}

	private void promijeniProcjenjeneVjerojatnostiGena(Kromosom pobjednik, Kromosom gubitnik, int velPopGA, double[] procjenjeneVjerojatnostiGena) {
		for (int i = 0; i < pobjednik.getBitovi().length; i++) {
			if (pobjednik.getBitovi()[i] != gubitnik.getBitovi()[i]) {
				if (pobjednik.getBitovi()[i] == (byte)1) procjenjeneVjerojatnostiGena[i] += 1.0/velPopGA;
				else procjenjeneVjerojatnostiGena[i] -= 1.0/velPopGA;
			}
		}
	}

	private boolean ispitajKonvergenciju(double[] procjenjeneVjerojatnostiGena, double ε) {
		for (int i = 0; i < procjenjeneVjerojatnostiGena.length; i++) {
			if (procjenjeneVjerojatnostiGena[i] > ε && procjenjeneVjerojatnostiGena[i] < (1-ε)) return true;
		}
		return false;
	}
	
	private Kromosom binarizirajVjerojatnosti (double[] procjenjeneVjerojatnostiGena, KromosomDekoder dekoder, double ε, Random rand) {
		Kromosom k = new Kromosom(dekoder);
		for (int i = 0; i < procjenjeneVjerojatnostiGena.length; i++) {
			if (procjenjeneVjerojatnostiGena[i] < ε) k.setBitovi(i, (byte)0);
			else if (procjenjeneVjerojatnostiGena[i] > (1-ε)) k.setBitovi(i, (byte)1);
			else if (rand.nextDouble() <= procjenjeneVjerojatnostiGena[i]) k.setBitovi(i, (byte)1);
			else k.setBitovi(i, (byte)0);
		}
		return k;
	}

	@Override
	public double[] getBestCodomainValuesPerIteration() {
		return bestCodomainValuesPerIteration;
	}

	@Override
	public String getAlgorithmName() {
		return algorithmName;
	}

	@Override
	public double getCodomainSolution() {
		return codomainSolution;
	}

	@Override
	public double[] getDomainSolution() {
		return domainSolution;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public int getBestVelueIteration() {
		return bestValueIteration;
	}
}