package hr.fer.zemris.dipl.knezevic.algorithms;

import java.util.Arrays;
import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.ISelection;

/**
 * Univariate Marginal Distribution Algorithm
 * EDAs univariante algorithm with independent variables.
 * Using discrete probability factorization, p(x) = Pi(1,n) {p(xi)}.
 * For parameter estimation using different types of smoothing (Laplace - λ=1, Jeffreys-Perks - λ=0.5, Schurmann-Grassberg - λ=1/K {K is 2 for binary cromosome}).
 * Few types of selection: roulette-wheel, k-tournament, n-best. 
 * @author Karlo Knezevic
 *
 */
public class UMDA extends Genetic {

	public UMDA() {
		algorithmName = "Univariate Marginal Distribution Algorithm";
	}

	@Override
	public void start(EDAParameters params) {
		startStopwatch();

		int VEL_POP = params.getVelPop();
		int brojJedinki = (int)Math.ceil(params.getUdioJedinkiZaEstimaciju()*VEL_POP);

		Random rand = new Random();

		KromosomDekoder dekoder = new KromosomDekoder(params.getBrojVarijabli(), params.getBrojBitovaPoVarijabli(), params.getxMin(), params.getxMax());

		Kromosom[] populacija = stvoriPopulaciju(VEL_POP, dekoder, rand);
		Kromosom[] selektiraneJedinke = stvoriPopulaciju(brojJedinki, dekoder, null);
		double[] procjenjeneVjerojatnostiGena = new double[params.getBrojVarijabli()*params.getBrojBitovaPoVarijabli()];

		IFunkcija funkcija = params.getFitnessFunkcija();
		ISelection selection = params.getSelection();

		Kromosom najbolji = null;
		evaluirajPopulaciju(populacija, funkcija);

		bestCodomainValuesPerIteration = new double[params.getBrojIteracija()];
		double minimum = 0;

		for (int generacija = 0; generacija < params.getBrojIteracija(); generacija++) {

			Arrays.sort(populacija);

			selection.selektirajJedinke(populacija, selektiraneJedinke);

			//UMDA learning
			procijeniVjerojatnostiGenaSelektiranihKromosoma(selektiraneJedinke, procjenjeneVjerojatnostiGena, params.getΛ());

			//u novu generaciju prenesi 2 najboljih jedinki; elitizam
			stvoriNovuGeneraciju(populacija, procjenjeneVjerojatnostiGena, rand, 2);

			evaluirajPopulaciju(populacija, funkcija);

			for (int i = 0; i < populacija.length; i++) {
				if (i==0 || najbolji.getFitnes() > populacija[i].getFitnes()) {
					najbolji = populacija[i];
				}
			}

			bestCodomainValuesPerIteration[generacija] = funkcija.izracunaj(najbolji.getVarijable());

			if (generacija == 0) {
				minimum = bestCodomainValuesPerIteration[generacija];
			} else {
				if (bestCodomainValuesPerIteration[generacija] < minimum) {
					bestValueIteration = generacija;
					minimum = bestCodomainValuesPerIteration[generacija];
				}
			}

		}

		domainSolution = najbolji.getVarijable();
		codomainSolution = funkcija.izracunaj(domainSolution);

		stopStopwatch();

		/*System.out.println(algorithmName + "|" + selection.getName() + "|" + funkcija.getName() + "| f(" + 
				Arrays.toString(domainSolution) + ") = " + funkcija.izracunaj(domainSolution) + "|" + executionTime + " ms | " + bestValueIteration);*/
	}

	@Override
	public Kromosom[] jezgra(Kromosom[] populacija, KromosomDekoder dekoder, EDAParameters params, Random rand) {

		Arrays.sort(populacija);

		int brojJedinki = (int)Math.ceil(params.getUdioJedinkiZaEstimaciju()*params.getVelPop());
		Kromosom[] selektiraneJedinke = stvoriPopulaciju(brojJedinki, dekoder, null);
		double[] procjenjeneVjerojatnostiGena = new double[params.getBrojVarijabli()*params.getBrojBitovaPoVarijabli()];
		Kromosom[] novaGeneracija = stvoriPopulaciju(params.getVelPop(), dekoder, null);

		ISelection selekcija = params.getSelection();
		selekcija.selektirajJedinke(populacija, selektiraneJedinke);

		procijeniVjerojatnostiGenaSelektiranihKromosoma(selektiraneJedinke, procjenjeneVjerojatnostiGena, params.getΛ());

		//elitizam
		novaGeneracija[0] = populacija[0];
		novaGeneracija[1] = populacija[1];

		stvoriNovuGeneraciju(novaGeneracija, procjenjeneVjerojatnostiGena, rand, 2);

		return novaGeneracija;
	}

	/**
	 * Using Laplace smoothing, λ=1
	 * @param selektiraneJedinke
	 * @param procjenjeneVjerojatnostiGena
	 */
	private void procijeniVjerojatnostiGenaSelektiranihKromosoma(Kromosom[] selektiraneJedinke, double[] procjenjeneVjerojatnostiGena, double λ) {
		Arrays.fill(procjenjeneVjerojatnostiGena, 0);
		for (int i = 0; i < selektiraneJedinke.length; i++) {
			for (int j = 0; j < procjenjeneVjerojatnostiGena.length; j++) {
				procjenjeneVjerojatnostiGena[j] += selektiraneJedinke[i].getBitovi()[j];
			}
		}

		double r = 2*λ;
		for (int i = 0; i < procjenjeneVjerojatnostiGena.length; i++) {
			procjenjeneVjerojatnostiGena[i] = (procjenjeneVjerojatnostiGena[i] + λ) / (selektiraneJedinke.length + r);
		}

	}

	private void stvoriNovuGeneraciju(Kromosom[] populacija, double[] procjenjeneVjerojatnostiGena, Random rand, int brojSacuvanihJedinki) {
		for (int i = brojSacuvanihJedinki; i < populacija.length; i++) {
			for (int j = 0; j < procjenjeneVjerojatnostiGena.length; j++) {
				if (rand.nextDouble() <= procjenjeneVjerojatnostiGena[j]) {
					populacija[i].setBitovi(j, (byte)1);
				} else {
					populacija[i].setBitovi(j, (byte)0);
				}
			}
		}
	}

	@Override
	public double[] getBestCodomainValuesPerIteration() {
		return bestCodomainValuesPerIteration;
	}

	@Override
	public String getAlgorithmName() {
		return algorithmName;
	}

	@Override
	public double getCodomainSolution() {
		return codomainSolution;
	}

	@Override
	public double[] getDomainSolution() {
		return domainSolution;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public int getBestVelueIteration() {
		return bestValueIteration;
	}
}