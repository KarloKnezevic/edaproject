package hr.fer.zemris.dipl.knezevic.algorithms;

import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;

public abstract class Genetic implements IAlgorithm {
	
	protected String algorithmName;
	protected double codomainSolution;
	protected double[] domainSolution;
	protected double[] bestCodomainValuesPerIteration;
	protected int bestValueIteration;
	protected long startTime;
	protected long endTime;
	protected long executionTime;
	
	protected Kromosom[] stvoriPopulaciju(int brojJedinki, KromosomDekoder dekoder, Random rand) {
		Kromosom[] populacija = new Kromosom[brojJedinki];
		for (int i = 0; i < populacija.length; i++) {
			if (rand == null) {
				populacija[i] = new Kromosom(dekoder);
			} else {
				populacija[i] = new Kromosom(dekoder, rand);
			}
		}
		return populacija;
	}
	
	protected void evaluirajPopulaciju (Kromosom[] populacija, IFunkcija funkcija) {
		for (int i = 0; i < populacija.length; i++) {
			evaluirajJedinku(populacija[i], funkcija);
		}
	}
	
	protected void evaluirajJedinku(Kromosom kromosom, IFunkcija funkcija) {
		kromosom.getDekoder().dekodirajKromosom(kromosom);
		kromosom.setFitnes(funkcija.izracunaj(kromosom.getVarijable()));
	}
	
	protected void startStopwatch() {
		startTime = System.currentTimeMillis();
	}
	
	protected void stopStopwatch() {
		endTime = System.currentTimeMillis();
		executionTime = endTime - startTime;
	}
	
	public Kromosom[] jezgra(Kromosom[] populacija, KromosomDekoder dekoder, EDAParameters params, Random rand) {
		return null;
	}
}