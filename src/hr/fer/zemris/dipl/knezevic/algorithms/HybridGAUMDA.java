package hr.fer.zemris.dipl.knezevic.algorithms;

import java.util.Arrays;
import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;

public class HybridGAUMDA extends Genetic {

	public HybridGAUMDA() {
		algorithmName = "Hybrid";
	}

	@Override
	public void start(EDAParameters params) {
		startStopwatch();

		int VEL_POP = params.getVelPop();
		Random rand = new Random();
		
		Genetic[] hybrids = params.getActiveHybridAlgorithm();

		KromosomDekoder dekoder = new KromosomDekoder(params.getBrojVarijabli(), params.getBrojBitovaPoVarijabli(), params.getxMin(), params.getxMax());

		Kromosom[] populacija = stvoriPopulaciju(VEL_POP, dekoder, rand);
		Kromosom[] novaPopulacija = stvoriPopulaciju(VEL_POP, dekoder, null);

		IFunkcija funkcija = params.getFitnessFunkcija();

		bestCodomainValuesPerIteration = new double[params.getBrojIteracija()];
		double minimum = 0;

		double udioJedinkiPrvePopulacije = 0.5;

		Kromosom najbolji = null;
		Kromosom[][] offsprings = new Kromosom[hybrids.length][];
		evaluirajPopulaciju(populacija, funkcija);
		
		for (int generacija = 0; generacija < params.getBrojIteracija(); generacija++) {
			
			Arrays.sort(populacija);
			
			//elitizam
			novaPopulacija[0] = populacija[0];
			novaPopulacija[1] = populacija[1];
			
			for (int i = 0; i < hybrids.length; i++) {
				offsprings[i] = hybrids[i].jezgra(populacija, dekoder, params, rand);
			}
						
			//elitizam, 2 najbolje jedinke se čuvaju
			stvoriNovuGeneraciju(offsprings, novaPopulacija, udioJedinkiPrvePopulacije, 2);
			udioJedinkiPrvePopulacije = ocijeniDobrotuGeneracija(offsprings, funkcija, udioJedinkiPrvePopulacije);
			
			populacija = novaPopulacija;
			evaluirajPopulaciju(populacija, funkcija);

			for (int i = 0; i < populacija.length; i++) {
				if (i==0 || najbolji.getFitnes() > populacija[i].getFitnes()) {
					najbolji = populacija[i];
				}
			}

			bestCodomainValuesPerIteration[generacija] = funkcija.izracunaj(najbolji.getVarijable());

			if (generacija == 0) {
				minimum = bestCodomainValuesPerIteration[generacija];
			} else {
				if (bestCodomainValuesPerIteration[generacija] < minimum) {
					bestValueIteration = generacija;
					minimum = bestCodomainValuesPerIteration[generacija];
				}
			}
		}

		domainSolution = najbolji.getVarijable();
		codomainSolution = funkcija.izracunaj(domainSolution);

		stopStopwatch();

		/*System.out.println(algorithmName + "|" + executionTime + " ms | " + bestValueIteration + "| ratio of " + 
				hybrids[0].getAlgorithmName() +": " + udioJedinkiPrvePopulacije*100+"%");*/

	}

	private void stvoriNovuGeneraciju(Kromosom[][] offsprings, Kromosom[] novaPopulacija, double udioJedinkiPrvePopulacije, int brojSacuvanihJedinki) {
		
		Kromosom[] populacija1 = offsprings[0];
		Kromosom[] populacija2 = offsprings[1];
		
		Arrays.sort(populacija1);
		Arrays.sort(populacija2);		
		
		int brojJedinkiPrvePopulacije = (int)Math.ceil(populacija1.length*udioJedinkiPrvePopulacije);
		for (int i = brojSacuvanihJedinki; i < brojJedinkiPrvePopulacije; i++) {
			novaPopulacija[i] = populacija1[i];
		}

		for (int i = 0; i < novaPopulacija.length-brojJedinkiPrvePopulacije; i++) {
			novaPopulacija[brojJedinkiPrvePopulacije+i] = populacija2[i];
		}
	}

	private double ocijeniDobrotuGeneracija(Kromosom[][] offsprings, IFunkcija funkcija, double ratio) {
		
		Kromosom[] generacija1 = offsprings[0];
		Kromosom[] generacija2 = offsprings[1];

		double[] avg = new double[2];
		int[] bestInGeneration = new int[avg.length];
		double ratioOfBest = 0.1;
		
		Kromosom[][] generacije = {generacija1, generacija2};
		
		for (int i = 0; i < generacije.length; i++) {
			evaluirajPopulaciju(generacije[i], funkcija);
			bestInGeneration[i] = (int)Math.ceil(generacije[i].length*ratioOfBest);
		}
		
		Arrays.fill(avg, 0);
		for (int i = 0; i < generacije.length; i++) {
			Arrays.sort(generacije[i]);
			for (int j = 0; j < bestInGeneration[i]; j++) {
				avg[i] += generacije[i][j].getFitnes();
			}
			avg[i] /= bestInGeneration[i];
		}
		
		double diff = (Math.min(avg[0], avg[1])) / (Math.max(avg[0], avg[1]));
		
		diff = diff>1 ? 1.0/diff : diff;
		
		if (avg[0] > avg[1]) {
			ratio = ratio - ratio*((1-diff)/3);
		} else if (avg[0] < avg[1]) {
			ratio = ratio + (1-ratio)*((1-diff)/3);
		}
		
		return ratio;
	}

	@Override
	public String getAlgorithmName() {
		return algorithmName;
	}

	@Override
	public double getCodomainSolution() {
		return codomainSolution;
	}

	@Override
	public double[] getDomainSolution() {
		return domainSolution;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public double[] getBestCodomainValuesPerIteration() {
		return bestCodomainValuesPerIteration;
	}

	@Override
	public int getBestVelueIteration() {
		return bestValueIteration;
	}
}