package hr.fer.zemris.dipl.knezevic.algorithms;

import hr.fer.zemris.dipl.knezevic.params.EDAParameters;

public interface IAlgorithm {

	public void start(EDAParameters params);
	public double[] getBestCodomainValuesPerIteration();
	public String getAlgorithmName();
	public double getCodomainSolution();
	public double[] getDomainSolution();
	public long getExecutionTime();
	public int getBestVelueIteration();

}
