package hr.fer.zemris.dipl.knezevic.algorithms;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;
import hr.fer.zemris.dipl.knezevic.chromosome.KromosomDekoder;
import hr.fer.zemris.dipl.knezevic.function.IFunkcija;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.ISelection;
import hr.fer.zemris.dipl.knezevic.util.InfoMeasurement;

public class MIMIC extends Genetic {

	public MIMIC() {
		algorithmName = "Mutual Information Maximization for Input Clustering";
	}

	@Override
	public void start(EDAParameters params) {
		startStopwatch();

		int VEL_POP = params.getVelPop();
		int brojJedinki = (int)Math.ceil(params.getUdioJedinkiZaEstimaciju()*VEL_POP);

		Random rand = new Random();

		KromosomDekoder dekoder = new KromosomDekoder(params.getBrojVarijabli(), params.getBrojBitovaPoVarijabli(), params.getxMin(), params.getxMax());

		Kromosom[] populacija = stvoriPopulaciju(VEL_POP, dekoder, rand);
		Kromosom[] selektiraneJedinke = stvoriPopulaciju(brojJedinki, dekoder, null);

		//∏ vektor određuje binarne ovisnosti varijabli
		int[] vektorPi = new int[params.getBrojVarijabli()*params.getBrojBitovaPoVarijabli()];

		IFunkcija funkcija = params.getFitnessFunkcija();
		ISelection selection = params.getSelection();

		Kromosom najbolji = null;
		evaluirajPopulaciju(populacija, funkcija);

		bestCodomainValuesPerIteration = new double[params.getBrojIteracija()];
		double minimum = 0;

		InfoMeasurement infoMeasurement = new InfoMeasurement();
		for (int generacija = 0; generacija < params.getBrojIteracija(); generacija++) {

			Arrays.sort(populacija);
			
			selection.selektirajJedinke(populacija, selektiraneJedinke);
			
			infoMeasurement.setData(getMatrix(selektiraneJedinke), params.getΛ());
			
			odrediVektorPi(infoMeasurement, vektorPi);
			
			//u novu generaciju prenesi 2 najboljih jedinki; elitizam
			infoMeasurement.stvoriNovuGeneraciju(vektorPi, populacija, 2);
			
			evaluirajPopulaciju(populacija, funkcija);
			
			for (int i = 0; i < populacija.length; i++) {
				if (i == 0 || najbolji.getFitnes() > populacija[i].getFitnes()) {
					najbolji = populacija[i];
				}
			}
			
			bestCodomainValuesPerIteration[generacija] = funkcija.izracunaj(najbolji.getVarijable());
			
			if (generacija == 0) {
				minimum = bestCodomainValuesPerIteration[generacija];
			} else {
				if (bestCodomainValuesPerIteration[generacija] < minimum) {
					bestValueIteration = generacija;
					minimum = bestCodomainValuesPerIteration[generacija];
				}
			}
		}
		
		domainSolution = najbolji.getVarijable();
		codomainSolution = funkcija.izracunaj(domainSolution);
		
		stopStopwatch();
		
		/*System.out.println(algorithmName + "|" + selection.getName() + "|" + funkcija.getName() + "| f(" + 
				Arrays.toString(domainSolution) + ") = " + funkcija.izracunaj(domainSolution) + "|" + executionTime + " ms | " + bestValueIteration);*/

	}


	private void odrediVektorPi(InfoMeasurement infoMeasurement, int[] vektorPi) {
		List<Integer> indexi = new LinkedList<>();
		for (int i = 0; i < vektorPi.length; i++) indexi.add(i);
		
		vektorPi[0] = infoMeasurement.computeMinGeneEntropy();
		indexi.remove(vektorPi[0]);

		int index = 0;
		double minEntropy = 0;
		for (int i = 0; i < vektorPi.length-1; i++) {
			for (int j = 0; j < indexi.size(); j++) {
				double computedEntropy = infoMeasurement.computeJointGeneEntropy(vektorPi[i], indexi.get(j));
				if (j==0) { minEntropy = computedEntropy; index = indexi.get(j); }
				if (computedEntropy < minEntropy) { minEntropy = computedEntropy; index = indexi.get(j); }
			}
			vektorPi[i+1] = index;
			indexi.remove((Object) index);
		}
	}

	private int[][] getMatrix(Kromosom[] selektiraneJedinke) {
		int[][] matrix = new int [selektiraneJedinke.length][selektiraneJedinke[0].getBitovi().length];
		for (int i = 0; i < selektiraneJedinke.length; i++) {
			for (int j = 0; j < selektiraneJedinke[i].getBitovi().length; j++) {
				matrix[i][j] = (int) selektiraneJedinke[i].getBitovi()[j];
			}
		}
		return matrix;
	}

	@Override
	public double[] getBestCodomainValuesPerIteration() {
		return bestCodomainValuesPerIteration;
	}

	@Override
	public String getAlgorithmName() {
		return algorithmName;
	}

	@Override
	public double getCodomainSolution() {
		return codomainSolution;
	}

	@Override
	public double[] getDomainSolution() {
		return domainSolution;
	}

	@Override
	public long getExecutionTime() {
		return executionTime;
	}

	@Override
	public int getBestVelueIteration() {
		return bestValueIteration;
	}
}