package hr.fer.zemris.dipl.knezevic.util;

import hr.fer.zemris.dipl.knezevic.chromosome.Kromosom;

import java.util.Arrays;
import java.util.Random;

public class InfoMeasurement {

	private final double log2 = 0.693147;

	private double λ;
	private int[][] data;
	private double[] geneProbabilities;
	private double[][][] geneJointProbabilities;

	public void setData(int[][] data, double λ) {
		this.λ = λ;
		this.data = data;
		geneProbabilities = new double[data[0].length];
		geneJointProbabilities = new double[data[0].length][data[0].length][4];

		computeGeneProbabilities();
		computeJointProbability();
	}

	private void computeGeneProbabilities() {
		Arrays.fill(geneProbabilities, 0);
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				geneProbabilities[j] += data[i][j];
			}
		}
		double r = 2*λ;
		for (int i = 0; i < geneProbabilities.length; i++) {
			geneProbabilities[i] = (geneProbabilities[i] + λ) / (data.length + r);
		}
	}

	private void computeJointProbability() {
		double[] jointProbability = new double[4];
		int j;
		for (int i = 0; i < data[0].length-1; i++) {
			for (j = i+1; j < data[0].length; j++) {
				Arrays.fill(jointProbability, 0);
				for (int k = 0; k < data.length; k++) {
					jointProbability[2*data[k][i] + data[k][j]]++;
				}

				for (int c = 0; c < 4; c++) {
					jointProbability[c] = (jointProbability[c] + λ) / (data.length + 2*λ);
				}
				geneJointProbabilities[i][j] = jointProbability;
				geneJointProbabilities[j][i] = geneJointProbabilities[i][j];
			}
			geneJointProbabilities[i][i] = null;
		}
	}

	public int computeMinGeneEntropy() {
		double geneEntropy;
		int index = 0;
		double minEntropy = 0;

		for (int i = 0; i < geneProbabilities.length; i++) {
			geneEntropy = -(1-geneProbabilities[i])*Math.log(1-geneProbabilities[i])/log2 - (geneProbabilities[i])*Math.log(geneProbabilities[i])/log2;
			if (i == 0) { minEntropy = geneEntropy; index = i; }
			if (geneEntropy < minEntropy) { index = i; }
		}
		return index;
	}

	//H(G1|G2)
	public double computeJointGeneEntropy(int gene1Index, int gene2Index) {
		double jointGeneEntropy = 0;
		double[] jointProbabilixtG1G2 = geneJointProbabilities[gene1Index][gene2Index];
		double[] probabilityG2 = {1-geneProbabilities[gene2Index], geneProbabilities[gene2Index]};

		for (int i = 0; i < probabilityG2.length; i++) {
			double sum = 0;
			for (int j = 0; j < jointProbabilixtG1G2.length; j++) {
				double yCx = jointProbabilixtG1G2[j]/probabilityG2[i];
				sum += yCx*Math.log(yCx)/log2;
			}
			jointGeneEntropy -= sum;
		}
		return jointGeneEntropy;
	}
	
	//n je broj kopirane djece
	public void stvoriNovuGeneraciju(int[] vektorPI, Kromosom[] populacija, int n) {
		Random rand = new Random();
		for (int i = n; i < populacija.length; i++) {
			if (rand.nextDouble() <= geneProbabilities[vektorPI[0]]) {
				populacija[i].setBitovi(vektorPI[0], (byte)1);
			} else {
				populacija[i].setBitovi(vektorPI[0], (byte)0);
			}
			
			for (int j = 1; j < vektorPI.length; j++) {
				double jointProb = (populacija[i].getBitovi()[vektorPI[j-1]] == (byte)1) ? geneJointProbabilities[vektorPI[j-1]][vektorPI[j]][3]: 
					geneJointProbabilities[vektorPI[j-1]][vektorPI[j]][2];
				double prob = jointProb / geneProbabilities[vektorPI[j-1]];
				if (rand.nextDouble() <= prob) {
					populacija[i].setBitovi(vektorPI[j], (byte)1);
				} else {
					populacija[i].setBitovi(vektorPI[j], (byte)0);
				}
			}
		}
	}
}