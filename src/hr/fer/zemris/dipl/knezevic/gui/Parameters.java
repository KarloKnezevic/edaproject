package hr.fer.zemris.dipl.knezevic.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import hr.fer.zemris.dipl.knezevic.function.FunctionFactory;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.SelectionFactory;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Parameters extends JPanel {
	private static final long serialVersionUID = 1L;

	public static EDAParameters params;

	public Parameters() {
		Parameters.params = new EDAParameters();
		initGUI();
	}

	private void initGUI() {
		setLayout(new BorderLayout());

		createLeft();
		createRight();
		createCenter();

	}

	private void createCenter() {
		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.CENTER);
		
		JPanel red1 = new JPanel(new BorderLayout());
		JLabel iterLabel = new JLabel("GA Mutation:");
		red1.add(iterLabel, BorderLayout.LINE_START);
		SpinnerModel genSpinnerModel = new SpinnerNumberModel(params.getVjerMut(), 0, 1, 0.005);
		final JSpinner genSpinner = new JSpinner(genSpinnerModel);
		genSpinner.setEditor(new JSpinner.NumberEditor(genSpinner,"0.000"));
		red1.add(genSpinner, BorderLayout.LINE_END);

		genSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setVjerMut((double) genSpinner.getValue());		
			}
		});

		JPanel red2 = new JPanel(new BorderLayout());
		JLabel jedLabel = new JLabel("GA Crossing:");
		red2.add(jedLabel, BorderLayout.LINE_START);
		SpinnerModel jedSpinnerModel = new SpinnerNumberModel(params.getVjerKriz(), 0, 1, 0.05);
		final JSpinner jedSpinner = new JSpinner(jedSpinnerModel);
		jedSpinner.setEditor(new JSpinner.NumberEditor(jedSpinner,"0.000"));
		red2.add(jedSpinner, BorderLayout.LINE_END);

		jedSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setVjerKriz((double) jedSpinner.getValue());
			}
		});

		JPanel red3 = new JPanel(new BorderLayout());
		JLabel varLabel = new JLabel("#CGA Individuals:");
		red3.add(varLabel, BorderLayout.LINE_START);
		SpinnerModel varSpinnerModel = new SpinnerNumberModel(params.getVelPopCGA(), 2, 100, 2);
		final JSpinner varSpinner = new JSpinner(varSpinnerModel);
		red3.add(varSpinner, BorderLayout.LINE_END);

		varSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setVelPopCGA((int) varSpinner.getValue());
			}
		});

		JPanel red4 = new JPanel(new BorderLayout());
		JLabel bitLabel = new JLabel("CGA ε Accept.:");
		red4.add(bitLabel, BorderLayout.LINE_START);
		SpinnerModel bitSpinnerModel = new SpinnerNumberModel(params.getΕ(), 0, 1, 0.0005);
		final JSpinner bitSpinner = new JSpinner(bitSpinnerModel);
		bitSpinner.setEditor(new JSpinner.NumberEditor(bitSpinner,"0.0000"));
		red4.add(bitSpinner, BorderLayout.LINE_END);

		bitSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setΕ((double) bitSpinner.getValue());
			}
		});

		JPanel red6 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabel = new JLabel("λ Smoothing:");
		red6.add(prostorPretrageLabel, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModel = new SpinnerNumberModel(params.getΛ(), 0, 1, 0.1);
		final JSpinner pPretrage = new JSpinner(prostorPretrageModel);
		pPretrage.setEditor(new JSpinner.NumberEditor(pPretrage,"0.00"));
		red6.add(pPretrage, BorderLayout.LINE_END);

		pPretrage.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setΛ((double) pPretrage.getValue());
			}
		});

		JPanel red7 = new JPanel(new BorderLayout());
		JLabel jedcgaLabel = new JLabel("PBIL α Learning:");
		red7.add(jedcgaLabel, BorderLayout.LINE_START);
		SpinnerModel jedcgaSpinnerModel = new SpinnerNumberModel(params.getΑ(), 0, 1, 0.05);
		final JSpinner jedcgaSpinner = new JSpinner(jedcgaSpinnerModel);
		jedcgaSpinner.setEditor(new JSpinner.NumberEditor(jedcgaSpinner,"0.00"));
		red7.add(jedcgaSpinner, BorderLayout.LINE_END);

		jedcgaSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setΑ((double) jedcgaSpinner.getValue());
			}
		});

		JPanel red9 = new JPanel(new BorderLayout());
		JLabel udioZaEstimLabel = new JLabel("PBIL Mutation Prob.:");
		red9.add(udioZaEstimLabel, BorderLayout.LINE_START);
		SpinnerModel udioZaEstimSpinnerModel = new SpinnerNumberModel(params.getMutationProbability(), 0, 1, 0.01);
		final JSpinner udioZaEstimSpinner = new JSpinner(udioZaEstimSpinnerModel);
		udioZaEstimSpinner.setEditor(new JSpinner.NumberEditor(udioZaEstimSpinner,"0.00"));
		red9.add(udioZaEstimSpinner, BorderLayout.LINE_END);

		udioZaEstimSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setMutationProbability((double)udioZaEstimSpinner.getValue());
			}
		});
		
		panel.add(red1);
		panel.add(red2);
		panel.add(new JSeparator());
		panel.add(red3);
		panel.add(red4);
		panel.add(new JSeparator());
		panel.add(red9);
		panel.add(red7);
		panel.add(new JSeparator());
		panel.add(red6);	

		
	}

	private void createRight() {
		JPanel panel = new JPanel(new GridLayout(0,2));
		add(panel, BorderLayout.LINE_END);
		
		//2. komponenta
		JLabel label = new JLabel("Choose algorithms:");
		JPanel red2 = new JPanel(new GridLayout(0,1));
		final JCheckBox[] boxevi = new JCheckBox[params.getAlgorithmCount()];
		red2.add(label);

		ItemListener checkboxListener = new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = 0;

				for (int i = 0; i < boxevi.length; i++) {
					if (boxevi[i] == e.getItemSelectable()) { index = i; break; }
				}

				if (e.getStateChange() == ItemEvent.SELECTED) {
					params.setActiveAlgorithm(index, true);
				}

				if (e.getStateChange() == ItemEvent.DESELECTED) {
					params.setActiveAlgorithm(index, false);
				}

			}
		};

		for (int i = 0; i < params.getAlgorithmCount(); i++) {
			boxevi[i] = new JCheckBox(params.getAlgorithmsName()[i]);
			boxevi[i].setSelected(true);
			boxevi[i].addItemListener(checkboxListener);
			red2.add(boxevi[i]);
		}

		//3. komponenta
		if (params.getHybridAlgorithmCount() != 0) {
			JPanel red3 = new JPanel(new GridLayout(0,1));
			JLabel labela = new JLabel("Algorithms for hybrid algorithm:");
			final JCheckBox[] boxevi2 = new JCheckBox[params.getHybridAlgorithmCount()];
			red3.add(labela);

			ItemListener chckbox2Listener = new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					int index = 0;

					for (int i = 0; i < boxevi2.length; i++) {
						if (boxevi2[i] == e.getItemSelectable()) { index = i; break; }
					}

					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (params.setActiveHybridAlgorithm(index, true) != 2) {
							params.setActiveAlgorithm(5, false);
							boxevi[5].setSelected(false);
							boxevi[5].setEnabled(false);
						} else {
							boxevi[5].setEnabled(true);
						}
					}

					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (params.setActiveHybridAlgorithm(index, false) != 2) {
							params.setActiveAlgorithm(5, false);
							boxevi[5].setSelected(false);
							boxevi[5].setEnabled(false);
						} else {
							boxevi[5].setEnabled(true);
						}
					}
				}
			};

			for (int i = 0; i < params.getHybridAlgorithmCount(); i++) {
				boxevi2[i] = new JCheckBox(params.getHybridAlgorithmNames()[i]);
				if (params.getHybridSelected()[i] == 1) {
					boxevi2[i].setSelected(true);
				}
				boxevi2[i].addItemListener(chckbox2Listener);
				red3.add(boxevi2[i]);
			}

			panel.add(red3);
		}
		panel.add(red2);
	}

	private void createLeft() {
		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.LINE_START);

		JPanel red1 = new JPanel(new BorderLayout());
		JLabel iterLabel = new JLabel("#Evaluation:");
		red1.add(iterLabel, BorderLayout.LINE_START);
		SpinnerModel genSpinnerModel = new SpinnerNumberModel(params.getBrojIteracija(), 1, 1000000, 100);
		final JSpinner genSpinner = new JSpinner(genSpinnerModel);
		red1.add(genSpinner, BorderLayout.LINE_END);

		genSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setBrojIteracija((int) genSpinner.getValue());		
			}
		});

		JPanel red2 = new JPanel(new BorderLayout());
		JLabel jedLabel = new JLabel("#Individuals:");
		red2.add(jedLabel, BorderLayout.LINE_START);
		SpinnerModel jedSpinnerModel = new SpinnerNumberModel(params.getVelPop(), 10, 1000, 10);
		final JSpinner jedSpinner = new JSpinner(jedSpinnerModel);
		red2.add(jedSpinner, BorderLayout.LINE_END);

		jedSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setVelPop((int) jedSpinner.getValue());
			}
		});

		JPanel red3 = new JPanel(new BorderLayout());
		JLabel varLabel = new JLabel("#Variable:");
		red3.add(varLabel, BorderLayout.LINE_START);
		SpinnerModel varSpinnerModel = new SpinnerNumberModel(params.getBrojVarijabli(), 1, 100, 1);
		final JSpinner varSpinner = new JSpinner(varSpinnerModel);
		red3.add(varSpinner, BorderLayout.LINE_END);

		varSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setBrojVarijabli((int) varSpinner.getValue());
			}
		});

		JPanel red4 = new JPanel(new BorderLayout());
		JLabel bitLabel = new JLabel("#Bit Per Variable:");
		red4.add(bitLabel, BorderLayout.LINE_START);
		SpinnerModel bitSpinnerModel = new SpinnerNumberModel(params.getBrojBitovaPoVarijabli(), 10, 100, 1);
		final JSpinner bitSpinner = new JSpinner(bitSpinnerModel);
		red4.add(bitSpinner, BorderLayout.LINE_END);

		bitSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setBrojBitovaPoVarijabli((int) bitSpinner.getValue());
			}
		});

		JPanel red5 = new JPanel(new BorderLayout());
		JLabel funcLabel = new JLabel("Fitness:");
		red5.add(funcLabel, BorderLayout.LINE_START);
		final JComboBox<String> fitnessImena = new JComboBox<String>(FunctionFactory.getFunctionNames());
		red5.add(fitnessImena);

		fitnessImena.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params.setFitnessFunkcija(FunctionFactory.getFunction((String)fitnessImena.getSelectedItem()));
			}
		});

		JPanel red6 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabel = new JLabel("Negative Domain:");
		red6.add(prostorPretrageLabel, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModel = new SpinnerNumberModel(params.getxMin(), -100000.0, 0.0, -10.5);
		final JSpinner pPretrage = new JSpinner(prostorPretrageModel);
		red6.add(pPretrage, BorderLayout.LINE_END);

		pPretrage.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setxMin((double) pPretrage.getValue());
			}
		});
		
		JPanel red10 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabelx = new JLabel("Positive Domain:");
		red10.add(prostorPretrageLabelx, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModelx = new SpinnerNumberModel(params.getxMax(), 0.0, 100000.0, 10.5);
		final JSpinner pPretragex = new JSpinner(prostorPretrageModelx);
		red10.add(pPretragex, BorderLayout.LINE_END);

		pPretragex.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setxMax((double) pPretragex.getValue());
			}
		});

		JPanel red7 = new JPanel(new BorderLayout());
		JLabel jedcgaLabel = new JLabel("#Test:");
		red7.add(jedcgaLabel, BorderLayout.LINE_START);
		SpinnerModel jedcgaSpinnerModel = new SpinnerNumberModel(params.getHowManyTestIters(), 1, 10000, 10);
		final JSpinner jedcgaSpinner = new JSpinner(jedcgaSpinnerModel);
		red7.add(jedcgaSpinner, BorderLayout.LINE_END);

		jedcgaSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setHowManyTestIters((int) jedcgaSpinner.getValue());
			}
		});

		JPanel red8 = new JPanel(new BorderLayout());
		JLabel selLabel = new JLabel("Selection:");
		red8.add(selLabel, BorderLayout.LINE_START);
		final JComboBox<String> selectionImena = new JComboBox<String>(SelectionFactory.getSelectionNames());
		red8.add(selectionImena);

		selectionImena.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params.setSelection(SelectionFactory.getSelection((String)selectionImena.getSelectedItem()));
			}
		});

		JPanel red9 = new JPanel(new BorderLayout());
		JLabel udioZaEstimLabel = new JLabel("%EDA Ind. Propor.:");
		red9.add(udioZaEstimLabel, BorderLayout.LINE_START);
		SpinnerModel udioZaEstimSpinnerModel = new SpinnerNumberModel((int)(params.getUdioJedinkiZaEstimaciju()*100), 0, 100, 5);
		final JSpinner udioZaEstimSpinner = new JSpinner(udioZaEstimSpinnerModel);
		red9.add(udioZaEstimSpinner, BorderLayout.LINE_END);

		udioZaEstimSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setUdioJedinkiZaEstimaciju((int)udioZaEstimSpinner.getValue()/100.0);
			}
		});

		panel.add(red1);
		panel.add(red2);
		panel.add(red3);
		panel.add(red4);
		panel.add(new JSeparator());
		panel.add(red9);
		panel.add(new JSeparator());
		panel.add(red7);
		panel.add(new JSeparator());
		panel.add(red5);
		panel.add(red6);
		panel.add(red10);
		panel.add(red8);	
	}
}