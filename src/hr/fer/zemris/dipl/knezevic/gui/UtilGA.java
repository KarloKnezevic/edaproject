package hr.fer.zemris.dipl.knezevic.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class UtilGA extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private double xMin = 0;
	private double xMax = 0;
	private int epsilon = -2;
	private int gNum = 0;
	
	public UtilGA() {
		initGUI();
	}

	private void initGUI() {
		setLayout(new BorderLayout());
		createLeft();
	}

	private void createLeft() {
		JPanel panel = new JPanel();
		add(panel, BorderLayout.LINE_START);
		
		JPanel red1 = new JPanel(new GridLayout(0, 1));
		
		JPanel xmin = new JPanel(new BorderLayout());
		JLabel xminLabel = new JLabel("Domain minimum:");
		xmin.add(xminLabel, BorderLayout.LINE_START);
		SpinnerModel xMinSpinnerModel = new SpinnerNumberModel(xMin, -5000000, 0, 500);
		final JSpinner xMinSpinner = new JSpinner(xMinSpinnerModel);
		xmin.add(xMinSpinner, BorderLayout.LINE_END);

		xMinSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				xMin = (double) xMinSpinner.getValue();
			}
		});
		
		JPanel xmax = new JPanel(new BorderLayout());
		JLabel xmaxLabel = new JLabel("Domain maximum:");
		xmax.add(xmaxLabel, BorderLayout.LINE_START);
		SpinnerModel xMaxSpinnerModel = new SpinnerNumberModel(xMax, 0, 5000000, 500);
		final JSpinner xMaxSpinner = new JSpinner(xMaxSpinnerModel);
		xmax.add(xMaxSpinner, BorderLayout.LINE_END);

		xMaxSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				xMax = (double) xMaxSpinner.getValue();
			}
		});
		
		JPanel eps = new JPanel(new BorderLayout());
		JLabel epsLabel = new JLabel("Precision: 10^");
		eps.add(epsLabel, BorderLayout.LINE_START);
		SpinnerModel epsSpinnerModel = new SpinnerNumberModel(epsilon, -20, 0, -1);
		final JSpinner epsSpinner = new JSpinner(epsSpinnerModel);
		eps.add(epsSpinner, BorderLayout.LINE_END);
		
		JPanel cnum = new JPanel(new BorderLayout());
		JLabel cnumLabel = new JLabel("#C. Genes:");
		cnum.add(cnumLabel, BorderLayout.LINE_START);
		SpinnerModel cnumSpinnerModel = new SpinnerNumberModel(gNum, 0, 200, 1);
		final JSpinner cnumSpinner = new JSpinner(cnumSpinnerModel);
		cnum.add(cnumSpinner, BorderLayout.LINE_END);

		cnumSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				gNum = (int) cnumSpinner.getValue();
			}
		});

		epsSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				epsilon = (int) epsSpinner.getValue();
			}
		});
		
		red1.add(xmin);
		red1.add(xmax);
		red1.add(eps);
		red1.add(cnum);
		red1.add(new JSeparator());
	
		JPanel red2 = new JPanel(new GridLayout(0,3));
		JButton button1 = new JButton("Calculate:");
		final JTextField field1 = new JTextField("Gene Number");
		final JTextField field2 = new JTextField("Precision");
		
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				field1.setText(calculateGenes()+"");
				field2.setText(calculatePrecision()+"");
			}
		});
		
		red2.add(button1);
		red2.add(field1);
		red2.add(field2);
		red1.add(red2);
		red1.add(new JSeparator());
		
		
		panel.add(red1);
		
	}
	
	private int calculateGenes() {
		if (xMin == xMax) return 0;
		double brojnik = Math.log((xMax-xMin)/Math.pow(10, epsilon));
		double nazivnik = Math.log(2);
		
		return (int)Math.ceil(brojnik/nazivnik);
	}
	
	private double calculatePrecision() {
		return Math.pow(Math.pow(2,gNum), -1)*(xMax-xMin);
	}
}