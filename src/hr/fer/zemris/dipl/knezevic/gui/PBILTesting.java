package hr.fer.zemris.dipl.knezevic.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import hr.fer.zemris.dipl.knezevic.algorithms.AlgorithmFactory;
import hr.fer.zemris.dipl.knezevic.algorithms.IAlgorithm;
import hr.fer.zemris.dipl.knezevic.function.FunctionFactory;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.SelectionFactory;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class PBILTesting extends JPanel {
	private static final long serialVersionUID = 1L;

	private EDAParameters params;

	private ChartPanel chartPanel;
	private XYDataset dataset;

	private JButton button = new JButton("Start Parameter Testing");
	private JProgressBar progress = new JProgressBar(1,100);

	private double from;
	private double to;
	private double step;

	private int selected = 0;

	public PBILTesting() {
		params = new EDAParameters();
		progress.setStringPainted(true);
		initGUI();
	}

	private void initGUI() {
		setLayout(new BorderLayout());

		createStart();

		createCenter();

		createLeft();

		createRight();
	}

	private void createStart() {
		add(button, BorderLayout.PAGE_START);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Worker().execute();
			}
		});
	}

	private void createCenter() {
		new Worker().execute();
		JFreeChart chart = createChart();
		chartPanel = new ChartPanel(chart);
		add(chartPanel, BorderLayout.CENTER);
	}
	
	private final void plotGraphCenter() {
		JFreeChart chart = createChart();
		chartPanel.setChart(chart);
	}

	private JFreeChart createChart() {
		XYDataset dataset = this.dataset;
		JFreeChart chart = ChartFactory.createXYLineChart(
				"Best Value Per Parameter",      
				"Parameter Value",                      	
				"Fitness Function Value",                 
				dataset,                  			
				PlotOrientation.VERTICAL,
				true,                     			
				true,                     			
				false                     			
				);

		XYPlot xyPlot = chart.getXYPlot();
		ValueAxis domainAxis = xyPlot.getDomainAxis();
		domainAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
		return chart;
	}

	private void createLeft() {
		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.LINE_START);

		JPanel red2 = new JPanel(new BorderLayout());
		JLabel jedLabel = new JLabel("#Individuals:");
		red2.add(jedLabel, BorderLayout.LINE_START);
		SpinnerModel jedSpinnerModel = new SpinnerNumberModel(params.getVelPop(), 10, 1000, 10);
		final JSpinner jedSpinner = new JSpinner(jedSpinnerModel);
		red2.add(jedSpinner, BorderLayout.LINE_END);

		jedSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setVelPop((int) jedSpinner.getValue());
			}
		});

		JPanel red3 = new JPanel(new BorderLayout());
		JLabel varLabel = new JLabel("#Variable:");
		red3.add(varLabel, BorderLayout.LINE_START);
		SpinnerModel varSpinnerModel = new SpinnerNumberModel(params.getBrojVarijabli(), 1, 100, 1);
		final JSpinner varSpinner = new JSpinner(varSpinnerModel);
		red3.add(varSpinner, BorderLayout.LINE_END);

		varSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setBrojVarijabli((int) varSpinner.getValue());
			}
		});

		JPanel red4 = new JPanel(new BorderLayout());
		JLabel bitLabel = new JLabel("#Bit Per Variable:");
		red4.add(bitLabel, BorderLayout.LINE_START);
		SpinnerModel bitSpinnerModel = new SpinnerNumberModel(params.getBrojBitovaPoVarijabli(), 10, 100, 1);
		final JSpinner bitSpinner = new JSpinner(bitSpinnerModel);
		red4.add(bitSpinner, BorderLayout.LINE_END);

		bitSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setBrojBitovaPoVarijabli((int) bitSpinner.getValue());
			}
		});

		JPanel red5 = new JPanel(new BorderLayout());
		JLabel funcLabel = new JLabel("Fitness:");
		red5.add(funcLabel, BorderLayout.LINE_START);
		final JComboBox<String> fitnessImena = new JComboBox<String>(FunctionFactory.getFunctionNames());
		red5.add(fitnessImena);

		fitnessImena.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params.setFitnessFunkcija(FunctionFactory.getFunction((String)fitnessImena.getSelectedItem()));
			}
		});

		JPanel red6 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabel = new JLabel("Negative Domain:");
		red6.add(prostorPretrageLabel, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModel = new SpinnerNumberModel(params.getxMin(), -100000.0, 0.0, -10.5);
		final JSpinner pPretrage = new JSpinner(prostorPretrageModel);
		red6.add(pPretrage, BorderLayout.LINE_END);

		pPretrage.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setxMin((double) pPretrage.getValue());
			}
		});

		JPanel red10 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabelx = new JLabel("Positive Domain:");
		red10.add(prostorPretrageLabelx, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModelx = new SpinnerNumberModel(params.getxMax(), 0.0, 100000.0, 10.5);
		final JSpinner pPretragex = new JSpinner(prostorPretrageModelx);
		red10.add(pPretragex, BorderLayout.LINE_END);

		pPretragex.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setxMax((double) pPretragex.getValue());
			}
		});

		JPanel red8 = new JPanel(new BorderLayout());
		JLabel selLabel = new JLabel("Selection:");
		red8.add(selLabel, BorderLayout.LINE_START);
		final JComboBox<String> selectionImena = new JComboBox<String>(SelectionFactory.getSelectionNames());
		red8.add(selectionImena);

		selectionImena.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params.setSelection(SelectionFactory.getSelection((String)selectionImena.getSelectedItem()));
			}
		});

		JPanel red9 = new JPanel(new BorderLayout());
		JLabel udioZaEstimLabel = new JLabel("%EDA Ind. Propor.:");
		red9.add(udioZaEstimLabel, BorderLayout.LINE_START);
		SpinnerModel udioZaEstimSpinnerModel = new SpinnerNumberModel((int)(params.getUdioJedinkiZaEstimaciju()*100), 0, 100, 5);
		final JSpinner udioZaEstimSpinner = new JSpinner(udioZaEstimSpinnerModel);
		red9.add(udioZaEstimSpinner, BorderLayout.LINE_END);

		udioZaEstimSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setUdioJedinkiZaEstimaciju((int)udioZaEstimSpinner.getValue()/100.0);
			}
		});

		JPanel red1 = new JPanel(new BorderLayout());
		JLabel iterLabel = new JLabel("#Evaluation:");
		red1.add(iterLabel, BorderLayout.LINE_START);
		SpinnerModel genSpinnerModel = new SpinnerNumberModel(params.getBrojIteracija(), 1, 1000000, 100);
		final JSpinner genSpinner = new JSpinner(genSpinnerModel);
		red1.add(genSpinner, BorderLayout.LINE_END);

		genSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setBrojIteracija((int) genSpinner.getValue());		
			}
		});

		JPanel red7 = new JPanel(new BorderLayout());
		JLabel crossing = new JLabel("Smoothnig:");
		red7.add(crossing, BorderLayout.LINE_START);
		SpinnerModel crossingModel = new SpinnerNumberModel(params.getΛ(), 0.0, 1000, 1.0);
		final JSpinner cross = new JSpinner(crossingModel);
		cross.setEditor(new JSpinner.NumberEditor(cross,"0.00"));
		red7.add(cross, BorderLayout.LINE_END);

		cross.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setΛ((double) cross.getValue());
			}
		});

		JPanel red11 = new JPanel(new BorderLayout());
		JLabel learning = new JLabel("Learning:");
		red11.add(learning, BorderLayout.LINE_START);
		SpinnerModel learningModel = new SpinnerNumberModel(params.getΑ(), 0.0, 1, 0.1);
		final JSpinner learn = new JSpinner(learningModel);
		learn.setEditor(new JSpinner.NumberEditor(learn,"0.00"));
		red11.add(learn, BorderLayout.LINE_END);

		learn.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setΑ((double) learn.getValue());
			}
		});

		panel.add(red9);
		panel.add(red11);
		panel.add(red7);
		panel.add(red1);
		panel.add(red2);
		panel.add(red3);
		panel.add(red4);
		panel.add(red5);
		panel.add(red6);
		panel.add(red10);
		panel.add(red8);
	}

	private void createRight() {
		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.LINE_END);

		JPanel red1 = new JPanel(new GridLayout(0,2));
		JRadioButton mutationButton = new JRadioButton("proportion");
		mutationButton.setSelected(true);
		JRadioButton smoothingButton = new JRadioButton("learning");
		ButtonGroup group = new ButtonGroup();
		group.add(mutationButton);
		group.add(smoothingButton);
		red1.add(mutationButton);
		red1.add(smoothingButton);

		mutationButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selected = 0;
			}
		});

		smoothingButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				selected = 1;
			}
		});

		JPanel red2 = new JPanel(new GridLayout(0,1));

		JPanel startLine = new JPanel(new BorderLayout());
		JLabel strt = new JLabel("Start:");
		startLine.add(strt, BorderLayout.LINE_START);
		SpinnerModel strtModel = new SpinnerNumberModel(params.getUdioJedinkiZaEstimaciju(), 0.0, 1.0, 0.1);
		from = params.getUdioJedinkiZaEstimaciju();
		final JSpinner poc = new JSpinner(strtModel);
		poc.setEditor(new JSpinner.NumberEditor(poc,"0.00"));
		startLine.add(poc, BorderLayout.LINE_END);

		poc.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				from = (double) poc.getValue();
			}
		});

		JPanel toLine = new JPanel(new BorderLayout());
		JLabel nd = new JLabel("End:");
		toLine.add(nd, BorderLayout.LINE_START);
		SpinnerModel endModel = new SpinnerNumberModel(params.getUdioJedinkiZaEstimaciju(), 0.0, 1.0, 0.1);
		to = params.getUdioJedinkiZaEstimaciju();
		final JSpinner kraj = new JSpinner(endModel);
		kraj.setEditor(new JSpinner.NumberEditor(kraj,"0.00"));
		toLine.add(kraj, BorderLayout.LINE_END);

		kraj.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				to = (double) kraj.getValue();
			}
		});

		JPanel stepLine = new JPanel(new BorderLayout());
		JLabel stp = new JLabel("Step:");
		stepLine.add(stp, BorderLayout.LINE_START);
		SpinnerModel stepModel = new SpinnerNumberModel(0, 0.0, 1.0, 0.05);
		final JSpinner korak = new JSpinner(stepModel);
		korak.setEditor(new JSpinner.NumberEditor(korak,"0.00"));
		stepLine.add(korak, BorderLayout.LINE_END);

		korak.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				step = (double) korak.getValue();
			}
		});

		red2.add(startLine);
		red2.add(toLine);
		red2.add(stepLine);

		JPanel red3 = new JPanel();
		red2.add(progress);

		panel.add(red1);
		panel.add(red2);
		panel.add(red3);

	}

	private class Worker extends SwingWorker<Void, Object> {

		@Override
		protected Void doInBackground() throws Exception {
			if (from >= to || step == 0.0) return null;

			button.setEnabled(false);

			EDAParameters parameters = new EDAParameters(params);
			IAlgorithm algoritam = AlgorithmFactory.getAlgorithm("Population-based incremental learning");
			XYSeries series = new XYSeries("Parameter");
			XYSeriesCollection dataset1 = new XYSeriesCollection();

			progress.setString(null);
			for(double i = from; i <= to; i += step) {
				algoritam.start(parameters);
				series.add(i, algoritam.getCodomainSolution());
				parameters = changeParameter(parameters, i);
				progress.setValue((int)((double)(i-from)*100.0/(to-from)));
			}
			progress.setValue(100);
			progress.setString("Testing done");
			dataset1.addSeries(series);
			dataset = dataset1;
			plotGraphCenter();

			button.setEnabled(true);
			return null;
		}
		
		private EDAParameters changeParameter(EDAParameters p, double i) {
			if (selected == 0) {
				p.setUdioJedinkiZaEstimaciju(step+i);
				return p;
			} else if (selected == 1) {
				p.setΑ(step+i);
				return p;
			}
			return p;
		}

	}

}
