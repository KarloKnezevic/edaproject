package hr.fer.zemris.dipl.knezevic.gui;

import hr.fer.zemris.dipl.knezevic.algorithms.IAlgorithm;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingWorker;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.TextAnchor;

public class AllTesting extends JPanel {
	private static final long serialVersionUID = 1L;

	private ChartPanel averageValueGraph;
	private ChartPanel bestValuePerTest;

	private XYDataset dataset;
	private CategoryDataset stat;

	private EDAParameters params;

	private DefaultTableModel model;
	
	private JButton button = new JButton("Start Testing");
	private JProgressBar progress = new JProgressBar(1,100);

	public AllTesting() {
		params = Parameters.params;
		model = new DefaultTableModel();
		progress.setStringPainted(true);
		initGUI();
	}

	private void initGUI() {
		setLayout(new BorderLayout());

		createStart();

		createCenter();

		createRight();

	}

	private double stdev(double[] array, double mean) {
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += (mean-array[i])*(mean-array[i]);
		}
		if (array.length == 0) return 0;
		return Math.sqrt(sum/array.length);
	}
	
	private void createStart() {
		button.setBackground(Color.CYAN);
		add(button, BorderLayout.PAGE_START);

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Worker().execute();
			}
		});
	}

	private final void plotGraphCenter() {
		XYDataset dataset = this.dataset;
		JFreeChart chart = ChartFactory.createXYLineChart(
				"Best Result Per Test",      
				"#Test",                      	
				"Fitness Function Value",                
				dataset,                  			
				PlotOrientation.VERTICAL,
				true,                     			
				true,                     		
				false                     			
				);
		XYPlot xyPlot = chart.getXYPlot();
		ValueAxis domainAxis = xyPlot.getDomainAxis();
		domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		bestValuePerTest.setChart(chart);
	}

	private void createCenter() {
		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.CENTER);

		new Worker().execute();
		XYDataset dataset = this.dataset;
		JFreeChart chart = ChartFactory.createXYLineChart(
				"Best Result Per Test",      
				"#Test",                      	
				"Fitness Function Value",                
				dataset,                  			
				PlotOrientation.VERTICAL,
				true,                     			
				true,                     		
				false                     			
				);
		XYPlot xyPlot = chart.getXYPlot();
		ValueAxis domainAxis = xyPlot.getDomainAxis();
		domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		bestValuePerTest = new ChartPanel(chart);
		panel.add(bestValuePerTest);


		JFreeChart chart1 = ChartFactory.createLineChart(
				"Average Value Per Algorithm",      
				"Algorithm",                      	
				"Average value",                
				stat,                  			
				PlotOrientation.VERTICAL,
				true,                     			
				true,                     		
				false                     			
				);
		CategoryPlot localCategoryPlot = (CategoryPlot)chart1.getPlot();
		NumberAxis localNumberAxis = (NumberAxis)localCategoryPlot.getRangeAxis();
		localNumberAxis.setAutoRangeIncludesZero(false);
		StatisticalBarRenderer localStatisticalBarRenderer = new StatisticalBarRenderer();
		localStatisticalBarRenderer.setDrawBarOutline(false);
		localStatisticalBarRenderer.setErrorIndicatorPaint(Color.black);
		localStatisticalBarRenderer.setIncludeBaseInRange(false);
		localCategoryPlot.setRenderer(localStatisticalBarRenderer);
		ChartUtilities.applyCurrentTheme(chart1);
		localStatisticalBarRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localStatisticalBarRenderer.setBaseItemLabelsVisible(true);
		localStatisticalBarRenderer.setBaseItemLabelPaint(Color.yellow);
		localStatisticalBarRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.INSIDE6, TextAnchor.BOTTOM_CENTER));
		GradientPaint localGradientPaint1 = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64));
		GradientPaint localGradientPaint2 = new GradientPaint(0.0F, 0.0F, Color.green, 0.0F, 0.0F, new Color(0, 64, 0));
		localStatisticalBarRenderer.setSeriesPaint(0, localGradientPaint1);
		localStatisticalBarRenderer.setSeriesPaint(1, localGradientPaint2);
		averageValueGraph = new ChartPanel(chart1);

		panel.add(averageValueGraph);
	}

	private void plotGraphLeft() {
		JFreeChart chart = ChartFactory.createLineChart(
				"Average Value Per Algorithm",      
				"Algorithm",                      	
				"Average value",                
				stat,                  			
				PlotOrientation.VERTICAL,
				true,                     			
				true,                     		
				false                     			
				);
		CategoryPlot localCategoryPlot = (CategoryPlot)chart.getPlot();
		NumberAxis localNumberAxis = (NumberAxis)localCategoryPlot.getRangeAxis();
		localNumberAxis.setAutoRangeIncludesZero(false);
		StatisticalBarRenderer localStatisticalBarRenderer = new StatisticalBarRenderer();
		localStatisticalBarRenderer.setDrawBarOutline(false);
		localStatisticalBarRenderer.setErrorIndicatorPaint(Color.black);
		localStatisticalBarRenderer.setIncludeBaseInRange(false);
		localCategoryPlot.setRenderer(localStatisticalBarRenderer);
		ChartUtilities.applyCurrentTheme(chart);
		localStatisticalBarRenderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
		localStatisticalBarRenderer.setBaseItemLabelsVisible(true);
		localStatisticalBarRenderer.setBaseItemLabelPaint(Color.yellow);
		localStatisticalBarRenderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.INSIDE6, TextAnchor.BOTTOM_CENTER));
		GradientPaint localGradientPaint1 = new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64));
		GradientPaint localGradientPaint2 = new GradientPaint(0.0F, 0.0F, Color.green, 0.0F, 0.0F, new Color(0, 64, 0));
		localStatisticalBarRenderer.setSeriesPaint(0, localGradientPaint1);
		localStatisticalBarRenderer.setSeriesPaint(1, localGradientPaint2);
		averageValueGraph.setChart(chart);
	}
	
	private void createRight() {
		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.LINE_END);

		JPanel red1 = new JPanel();
		JTable table = new JTable(model);
		red1.add(table);
		
		JPanel red2 = new JPanel();
		red2.add(progress);

		panel.add(red1);
		panel.add(red2);

	}

	private void updateTable(IAlgorithm[] algorithmData, double[] avg, double[] stddev) {
		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("Col1");
		model.addColumn("Col2");
		model.addColumn("Col3");

		model.addRow(new Object[] {"Algorithm", "Average", "Deviation"});
		for(int i = 0; i < algorithmData.length; i++) {
			model.addRow(new Object[] {algorithmData[i].getAlgorithmName(), avg[i], 
					stddev[i]});
		}
	}
	
	private class Worker extends SwingWorker<Void, Object> {

		@Override
		protected Void doInBackground() throws Exception {
			
			button.setEnabled(false);
			
			params = Parameters.params;
			int brojTestova = params.getHowManyTestIters();
			IAlgorithm[] algorithms = params.getAlgoritmi();
			XYSeriesCollection dataset1 = new XYSeriesCollection();
			DefaultStatisticalCategoryDataset statData = new DefaultStatisticalCategoryDataset();
			XYSeries series = null;
			double[] stdev = new double[algorithms.length];
			double[] avgar = new double[algorithms.length];

			progress.setString(null);
			progress.setValue(0);
			//prođi kroz sve algoritme
			for (int algoritam = 0; algoritam < algorithms.length; algoritam++) {
				double avg = 0;
				double[] vrijednosti = new double[brojTestova];

				//pokreni algoritam test puta
				series = new XYSeries(algorithms[algoritam].getAlgorithmName());
				for (int test = 0; test < brojTestova; test++) {
					algorithms[algoritam].start(params);
					series.add(test, algorithms[algoritam].getCodomainSolution());
					avg += algorithms[algoritam].getCodomainSolution();
					vrijednosti[test] = algorithms[algoritam].getCodomainSolution();
					
					progress.setValue((int)((double)(algoritam*brojTestova + test + 1)*100.0 / (algorithms.length*brojTestova)));
					
				}
				dataset1.addSeries(series);
				avgar[algoritam] = avg/brojTestova;
				stdev[algoritam] = stdev(vrijednosti, avg/brojTestova);
				statData.add(avgar[algoritam], stdev[algoritam], algorithms[algoritam].getAlgorithmName(), algorithms[algoritam].getAlgorithmName());
			}
			
			progress.setString("Testing done");
			
			//osvježi podatke
			updateTable(algorithms, avgar, stdev);
			dataset = dataset1;
			stat = statData;
			plotGraphCenter();
			plotGraphLeft();
			
			button.setEnabled(true);
			return null;
		}
		
		
	}
}