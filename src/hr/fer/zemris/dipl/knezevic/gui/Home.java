package hr.fer.zemris.dipl.knezevic.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Home extends JPanel {
	private static final long serialVersionUID = 1L;
	
	public Home() {
		initGUI();
	}

	private void initGUI() {
		setLayout(new BorderLayout());
		createInfo();
	}

	private void createInfo() {
		JPanel panel = new JPanel(new GridLayout(0,1));
		add(panel, BorderLayout.CENTER);
		
		JPanel red4 = new JPanel();
		JLabel l4 = new JLabel("SVEUČILIŠTE U ZAGREBU");
		red4.add(l4);
		
		JPanel red0 = new JPanel();
		JLabel l0 = new JLabel("FAKULTET ELEKTROTEHNIKE I RAČUNARSTVA");
		red0.add(l0);
		
		JPanel red5 = new JPanel();
		JLabel l5 = new JLabel("ZAVOD ZA ELEKTRONIKU, MIKROELEKTRONIKU, RAČUNALNE I INTELIGENTNE SUSTAVE");
		red5.add(l5);
		
		JPanel red1 = new JPanel();
		JLabel l1 = new JLabel("DIPLOMSKI PROJEKT");
		red1.add(l1);
		
		JPanel red2 = new JPanel();
		JLabel l2 = new JLabel("IZGRADNJA GENETSKOG ALGORITMA UPORABOM VJEROJATNOSNIH RAZDIOBA");
		red2.add(l2);
		
		JPanel red3 = new JPanel();
		JLabel l3 = new JLabel("KARLO KNEŽEVIĆ");
		red3.add(l3);
		
		panel.add(red4);
		panel.add(red0);
		panel.add(red5);
		panel.add(red1);
		panel.add(red2);
		panel.add(red3);
		
	}
}