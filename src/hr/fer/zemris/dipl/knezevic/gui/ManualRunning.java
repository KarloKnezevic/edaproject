package hr.fer.zemris.dipl.knezevic.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import hr.fer.zemris.dipl.knezevic.algorithms.IAlgorithm;
import hr.fer.zemris.dipl.knezevic.function.FunctionFactory;
import hr.fer.zemris.dipl.knezevic.params.EDAParameters;
import hr.fer.zemris.dipl.knezevic.selection.SelectionFactory;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ManualRunning extends JPanel {
	private static final long serialVersionUID = 1L;

	private ChartPanel chartPanel;
	private JTextArea info;
	private DefaultTableModel model;
	private EDAParameters params;

	private XYDataset dataset;

	private JButton button = new JButton("Recalculate");
	private JProgressBar progress = new JProgressBar(1,100);

	private boolean showDomSol = false;

	public ManualRunning() {
		params = new EDAParameters();
		model = new DefaultTableModel();
		progress.setStringPainted(true);
		initGUI();
	}

	private void initGUI() {
		setLayout(new BorderLayout());

		createStart();

		createCenter();

		createLeft();

		createRight();

		createEnd();
	}

	private void createRight() {

		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.LINE_END);

		//1. komponenta
		JPanel red1 = new JPanel(new GridLayout(0,1));
		JTable table = new JTable(model);
		red1.add(table);

		//2. komponenta
		JLabel label = new JLabel("Choose algorithms:");
		JPanel red2 = new JPanel(new GridLayout(0,1));
		final JCheckBox[] boxevi = new JCheckBox[params.getAlgorithmCount()];
		red2.add(label);

		ItemListener checkboxListener = new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				int index = 0;

				for (int i = 0; i < boxevi.length; i++) {
					if (boxevi[i] == e.getItemSelectable()) { index = i; break; }
				}

				if (e.getStateChange() == ItemEvent.SELECTED) {
					params.setActiveAlgorithm(index, true);
				}

				if (e.getStateChange() == ItemEvent.DESELECTED) {
					params.setActiveAlgorithm(index, false);
				}

			}
		};

		for (int i = 0; i < params.getAlgorithmCount(); i++) {
			boxevi[i] = new JCheckBox(params.getAlgorithmsName()[i]);
			boxevi[i].setSelected(true);
			boxevi[i].addItemListener(checkboxListener);
			red2.add(boxevi[i]);
		}

		//3. komponenta
		if (params.getHybridAlgorithmCount() != 0) {
			JPanel red3 = new JPanel(new GridLayout(0,1));
			JLabel labela = new JLabel("Algorithms for hybrid algorithm:");
			final JCheckBox[] boxevi2 = new JCheckBox[params.getHybridAlgorithmCount()];
			red3.add(labela);

			ItemListener chckbox2Listener = new ItemListener() {
				@Override
				public void itemStateChanged(ItemEvent e) {
					int index = 0;

					for (int i = 0; i < boxevi2.length; i++) {
						if (boxevi2[i] == e.getItemSelectable()) { index = i; break; }
					}

					if (e.getStateChange() == ItemEvent.SELECTED) {
						if (params.setActiveHybridAlgorithm(index, true) != 2) {
							params.setActiveAlgorithm(5, false);
							boxevi[5].setSelected(false);
							boxevi[5].setEnabled(false);
						} else {
							boxevi[5].setEnabled(true);
						}
					}

					if (e.getStateChange() == ItemEvent.DESELECTED) {
						if (params.setActiveHybridAlgorithm(index, false) != 2) {
							params.setActiveAlgorithm(5, false);
							boxevi[5].setSelected(false);
							boxevi[5].setEnabled(false);
						} else {
							boxevi[5].setEnabled(true);
						}
					}
				}
			};

			for (int i = 0; i < params.getHybridAlgorithmCount(); i++) {
				boxevi2[i] = new JCheckBox(params.getHybridAlgorithmNames()[i]);
				if (params.getHybridSelected()[i] == 1) {
					boxevi2[i].setSelected(true);
				}
				boxevi2[i].addItemListener(chckbox2Listener);
				red3.add(boxevi2[i]);
			}

			panel.add(red3);
		}

		ItemListener showDomListener = new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					showDomSol = true;
				}

				if (e.getStateChange() == ItemEvent.DESELECTED) {
					showDomSol = false;
				}

			}
		};

		//komponenta
		JPanel red5 = new JPanel();
		JCheckBox showDomain = new JCheckBox("Show Domain Solutions");
		showDomain.setSelected(false);
		showDomain.addItemListener(showDomListener);
		red5.add(showDomain);

		JPanel red4 = new JPanel(new BorderLayout());
		red4.add(progress, BorderLayout.PAGE_START);
		red4.add(red5, BorderLayout.LINE_START);

		panel.add(red1);
		panel.add(red2);
		panel.add(red4);
		
	}

	private void updateTable(IAlgorithm[] algorithmData) {
		model.setRowCount(0);
		model.setColumnCount(0);

		model.addColumn("Col1");
		model.addColumn("Col2");
		model.addColumn("Col3");
		model.addColumn("Col4");

		model.addRow(new Object[] {"Algorithm", "Optimum", "Exec time [ms]", "Iter for opt"});
		for(int i = 0; i < algorithmData.length; i++) {
			model.addRow(new Object[] {algorithmData[i].getAlgorithmName(), algorithmData[i].getCodomainSolution(), 
					algorithmData[i].getExecutionTime(), algorithmData[i].getBestVelueIteration()});
		}
	}

	private void createEnd() {
		info = new JTextArea();
		updateInfo();
		info.setEditable(false);
		add(info, BorderLayout.PAGE_END);
	}

	private void updateInfo() {
		info.setText(params.getFitnessFunkcija().getInfo());
	}

	private void createLeft() {

		JPanel panel = new JPanel(new GridLayout(0, 1));
		add(panel, BorderLayout.LINE_START);

		JPanel red1 = new JPanel(new BorderLayout());
		JLabel iterLabel = new JLabel("#Evaluation:");
		red1.add(iterLabel, BorderLayout.LINE_START);
		SpinnerModel genSpinnerModel = new SpinnerNumberModel(params.getBrojIteracija(), 1, 1000000, 100);
		final JSpinner genSpinner = new JSpinner(genSpinnerModel);
		red1.add(genSpinner, BorderLayout.LINE_END);

		genSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setBrojIteracija((int) genSpinner.getValue());		
			}
		});

		JPanel red2 = new JPanel(new BorderLayout());
		JLabel jedLabel = new JLabel("#Individuals:");
		red2.add(jedLabel, BorderLayout.LINE_START);
		SpinnerModel jedSpinnerModel = new SpinnerNumberModel(params.getVelPop(), 10, 1000, 10);
		final JSpinner jedSpinner = new JSpinner(jedSpinnerModel);
		red2.add(jedSpinner, BorderLayout.LINE_END);

		jedSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setVelPop((int) jedSpinner.getValue());
			}
		});

		JPanel red3 = new JPanel(new BorderLayout());
		JLabel varLabel = new JLabel("#Variable:");
		red3.add(varLabel, BorderLayout.LINE_START);
		SpinnerModel varSpinnerModel = new SpinnerNumberModel(params.getBrojVarijabli(), 1, 100, 1);
		final JSpinner varSpinner = new JSpinner(varSpinnerModel);
		red3.add(varSpinner, BorderLayout.LINE_END);

		varSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setBrojVarijabli((int) varSpinner.getValue());
			}
		});

		JPanel red4 = new JPanel(new BorderLayout());
		JLabel bitLabel = new JLabel("#Bit Per Variable:");
		red4.add(bitLabel, BorderLayout.LINE_START);
		SpinnerModel bitSpinnerModel = new SpinnerNumberModel(params.getBrojBitovaPoVarijabli(), 10, 100, 1);
		final JSpinner bitSpinner = new JSpinner(bitSpinnerModel);
		red4.add(bitSpinner, BorderLayout.LINE_END);

		bitSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				params.setBrojBitovaPoVarijabli((int) bitSpinner.getValue());
			}
		});

		JPanel red5 = new JPanel(new BorderLayout());
		JLabel funcLabel = new JLabel("Fitness:");
		red5.add(funcLabel, BorderLayout.LINE_START);
		final JComboBox<String> fitnessImena = new JComboBox<String>(FunctionFactory.getFunctionNames());
		red5.add(fitnessImena);

		fitnessImena.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params.setFitnessFunkcija(FunctionFactory.getFunction((String)fitnessImena.getSelectedItem()));
			}
		});

		JPanel red6 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabel = new JLabel("Negative Domain:");
		red6.add(prostorPretrageLabel, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModel = new SpinnerNumberModel(params.getxMin(), -100000.0, 0.0, -10.5);
		final JSpinner pPretrage = new JSpinner(prostorPretrageModel);
		red6.add(pPretrage, BorderLayout.LINE_END);

		pPretrage.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setxMin((double) pPretrage.getValue());
			}
		});

		JPanel red10 = new JPanel(new BorderLayout());
		JLabel prostorPretrageLabelx = new JLabel("Positive Domain:");
		red10.add(prostorPretrageLabelx, BorderLayout.LINE_START);
		SpinnerModel prostorPretrageModelx = new SpinnerNumberModel(params.getxMax(), 0.0, 100000.0, 10.5);
		final JSpinner pPretragex = new JSpinner(prostorPretrageModelx);
		red10.add(pPretragex, BorderLayout.LINE_END);

		pPretragex.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setxMax((double) pPretragex.getValue());
			}
		});

		JPanel red7 = new JPanel(new BorderLayout());
		JLabel jedcgaLabel = new JLabel("#CGA Individuals:");
		red7.add(jedcgaLabel, BorderLayout.LINE_START);
		SpinnerModel jedcgaSpinnerModel = new SpinnerNumberModel(params.getVelPopCGA(), 2, 50, 1);
		final JSpinner jedcgaSpinner = new JSpinner(jedcgaSpinnerModel);
		red7.add(jedcgaSpinner, BorderLayout.LINE_END);

		jedcgaSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setVelPopCGA((int) jedcgaSpinner.getValue());
			}
		});

		JPanel red8 = new JPanel(new BorderLayout());
		JLabel selLabel = new JLabel("Selection:");
		red8.add(selLabel, BorderLayout.LINE_START);
		final JComboBox<String> selectionImena = new JComboBox<String>(SelectionFactory.getSelectionNames());
		red8.add(selectionImena);

		selectionImena.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				params.setSelection(SelectionFactory.getSelection((String)selectionImena.getSelectedItem()));
			}
		});

		JPanel red9 = new JPanel(new BorderLayout());
		JLabel udioZaEstimLabel = new JLabel("%EDA Ind. Propor.:");
		red9.add(udioZaEstimLabel, BorderLayout.LINE_START);
		SpinnerModel udioZaEstimSpinnerModel = new SpinnerNumberModel((int)(params.getUdioJedinkiZaEstimaciju()*100), 0, 100, 5);
		final JSpinner udioZaEstimSpinner = new JSpinner(udioZaEstimSpinnerModel);
		red9.add(udioZaEstimSpinner, BorderLayout.LINE_END);

		udioZaEstimSpinner.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {
				params.setUdioJedinkiZaEstimaciju((int)udioZaEstimSpinner.getValue()/100.0);
			}
		});

		panel.add(red1);
		panel.add(red2);
		panel.add(red3);
		panel.add(red9);
		panel.add(red7);
		panel.add(red4);
		panel.add(red5);
		panel.add(red6);
		panel.add(red10);
		panel.add(red8);
	}

	private void createCenter() {
		new Worker().execute();
		button.setEnabled(true);
		JFreeChart chart = createChart();
		chartPanel = new ChartPanel(chart);
		add(chartPanel, BorderLayout.CENTER);
	}

	private void createStart() {
		button.setBackground(Color.CYAN);
		add(button, BorderLayout.PAGE_START);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Worker().execute();
			}
		});
	}

	private final void plotGraph() {
		JFreeChart chart = createChart();
		chartPanel.setChart(chart);
	}

	private JFreeChart createChart() {
		XYDataset dataset = this.dataset;
		JFreeChart chart = ChartFactory.createXYLineChart(
				"Best Value Per Generation",      
				"#Generation",                      	
				"Fitness Function Value",                 
				dataset,                  			
				PlotOrientation.VERTICAL,
				true,                     			
				true,                     			
				false                     			
				);

		XYPlot xyPlot = chart.getXYPlot();
		ValueAxis domainAxis = xyPlot.getDomainAxis();
		domainAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		return chart;
	}

	private class Worker extends SwingWorker<Void, Object> {

		@Override
		protected Void doInBackground() throws Exception {
			button.setEnabled(false);

			IAlgorithm[] algorithms = params.getAlgoritmi();
			XYSeriesCollection dataset1 = new XYSeriesCollection();
			XYSeries series;

			progress.setString(null);
			progress.setValue(0);
			for (int i = 0; i < algorithms.length; i++) {
				algorithms[i].start(params);
				series = new XYSeries(algorithms[i].getAlgorithmName());
				double[] yValues = algorithms[i].getBestCodomainValuesPerIteration();
				for (int j = 0; j < yValues.length; j++) {
					series.add(j, yValues[j]);
				}
				dataset1.addSeries(series);
				progress.setValue((int)((double)((i+1)*100.0/algorithms.length)));
			}

			progress.setString("Done");
			updateTable(algorithms);
			dataset = dataset1;
			plotGraph();
			updateInfo();

			if (showDomSol) {
				ispisiNajboljeVrijednosti(algorithms);
			}

			button.setEnabled(true);
			return null;
		}

		private void ispisiNajboljeVrijednosti(IAlgorithm[] algorithms) {
			StringBuilder sb = new StringBuilder(200);
			sb.append("==Domain values for best fitness==");
			sb.append("\r\n");
			for (int i = 0; i < algorithms.length; i++) {
				sb.append((i+1) + ".] " + algorithms[i].getAlgorithmName() + "\r\n");
				for (int j = 0; j < algorithms[i].getDomainSolution().length; j++) {
					sb.append("x"+j+" = " + algorithms[i].getDomainSolution()[j] + "\r\n");
				}
			}
			String s = sb.toString();
			JOptionPane.showMessageDialog(null, s, "Solutions", JOptionPane.INFORMATION_MESSAGE);
		}

	}
}