package hr.fer.zemris.dipl.knezevic.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

public class GUIWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	private JTabbedPane tabbedPaneMain;
	private JTabbedPane tabbedPaneTesting;
	private JTabbedPane tabbedPaneAllAndParams;

	public GUIWindow() {
		initGUI();
	}

	private void initGUI() {
		setLocation(0, 0);
		setSize(1200,550);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Estimation of distribution algorithm");
		getContentPane().setLayout(new BorderLayout());
		/*try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ignorable) {}*/
		
		tabbedPaneAllAndParams = new JTabbedPane();
		tabbedPaneAllAndParams.add("Parameters", new Parameters());
		tabbedPaneAllAndParams.add("Test", new AllTesting());

		tabbedPaneTesting = new JTabbedPane();
		tabbedPaneTesting.add("All Algorithms", tabbedPaneAllAndParams);
		tabbedPaneTesting.add("GA", new GATesting());
		tabbedPaneTesting.add("UMDA", new UMDATesting());
		tabbedPaneTesting.add("PBIL", new PBILTesting());
		tabbedPaneTesting.add("CGA", new CGATesting());
		tabbedPaneTesting.add("MIMIC", new MIMICTesting());
		//tabbedPaneTesting.add("Hybrid", null);
		
		tabbedPaneMain = new JTabbedPane();
		tabbedPaneMain.add("Home", new Home());
		tabbedPaneMain.add("Manual Running", new ManualRunning());
		tabbedPaneMain.add("Testing", tabbedPaneTesting);
		tabbedPaneMain.add("Utility", new UtilGA());
		
		getContentPane().add(tabbedPaneMain);
	}
}